<?php get_header(); ?>

<style type="text/css">

.acf-map {
	width: 100%;
	min-height: 400px;
	margin: 20px 0;
	border: 1px solid #fff;
}
@media (max-width: 620px) {
	.acf-map {
		width: 100%;
		min-height: 400px;
	}
}
/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
			
			
			
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						//page views
						wpb_set_post_views(get_the_ID());

						//get banner
						$src = get_post_banner();
					?>
					
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> >
						<header>
							<div class="row">
								<div class="medium-12 columns">
									<h1 itemprop="headline" class="tb-pad-60"><?php the_title(); ?></h1>
								</div>
							</div>
						</header>
						<div id="location-information">
							<div class="row">
								<div class="medium-6 columns">
									<div class="phone-number">
										<h1><?php
											$shortcode_str = '[frn_phone ga_phone_location="Phone Clicks in Location Detail" number="'.display_phone_number(get_field('phone_number')).'"]';

											echo  do_shortcode($shortcode_str) ?></h1>
									</div>
									<div class="intro-text tb-pad-20">
										<section>
											<p><?php the_field('intro_content') ?></p>
										</section>
									</div>
									<div class="buttons-shell">
										
										<div class="row">
											<div class="large-8 columns ">
												<?php 
												$email = get_field('contact_email_address');
												echo do_shortcode('[lhn_inpage button="email" text="send us an email" class="button hollow expanded large"]');
												echo do_shortcode('[lhn_inpage button="chat" text="chat with us now" offline="remove" class="button hollow expanded large"]');
												?>
												<?php /* Old version of chat removed 2/17/19
												<div><a href="#chat" class="button hollow expanded large chat-launch" >chat with us now</a></div>
												*/ 	?>
												<?php if(get_field('brochure')): ?>
												<div><a href="<?php the_field('brochure') ?>" target="_blank" class="button hollow expanded large" >brochure</a></div>
												<?php endif; ?>
											</div>
										</div>
									
									</div>
								</div>
								<div class="medium-6 columns">
										<div class="map-shell">
											<?php 

											$location = get_field('location');

											if( !empty($location) ):
											?>
											<div class="acf-map">
												<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
												
														<div class="map-marker">
															<h4><?php the_title() ?> Location</h4>
															<address>
																<div class="address-one">
																	<?php the_field('street_address') ?>
																</div>
																<?php if(the_field('street_address_two')): ?>
																	<div class="address-two">
																		<?php the_field('street_address_two') ?>
																	</div>
																<?php endif; ?>
																<div class="address-three">
																	<?php the_field('city') ?>, <?php the_field('state') ?> <?php the_field('zipcode') ?>
																</div>
															</address>
															
														</div>
												
												</div>
											</div>
											<?php endif; ?>
										</div>								
								</div>
							</div>
						</div>
						
						<div id="location-programs">
							<div class="clearfix row">
								
								<div id="main" class="large-12 columns tb-pad-60 float-left" role="main" >
									<section class="post_content clearfix" itemprop="articleBody">
						
										<?php the_content(); ?>
																	
										

										<?php wp_link_pages(); ?>
										
										
										<?php 
										// only show edit button if user has permission to edit posts
										if( $user_level > 0 ) { 
										?>
											<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","wpbootstrap"); ?></a>
										<?php } ?>
									</section> <!-- end article section -->
								</div> <!-- end #main -->
									
							</div>
						</div>
						
							<?php 
							$staff_members = get_staff_members_from_location();
							if($staff_members):?>
								<div class="staff-members location-page">
									<div class="row tb-pad-60">
										<div class="large-12 columns">
											<h2 class="text-center">Our Team</h2>
											<div class="row tb-pad-30 small-up-1 medium-up-2 large-up-5">
												<?php
												foreach( $staff_members as $sm){
													?> 
														<div class="column">
														<a href="<?php echo $sm['permalink'] ?>">
															<img src="<?php echo $sm['image'] ?>" alt="<?php echo $sm['title'] ?>" />
															<div class="location-staff-title text-center">
																<h4 style=""><?php echo $sm['title'] ?></h4>
																<h5 style=""><?php echo $sm['job_title'] ?></h5>
															</div>
														</a>
														</div>
													<?php
												} ?>
											</div>	<!-- row -->	
										</div>
									</div>
								</div>	<!-- staff member -->	
							<?php endif; ?>	
						
						<?php 
						$optional_photo_gallery = get_field('optional_photo_gallery');
						if($optional_photo_gallery):?>
							<div class="optional-photo-gallery tb-pad-60">
								<div class="row">
									<div class="medium-12 columns">
										<?php the_field('optional_photo_gallery'); ?>
									</div>
								</div>	<!-- row -->	
							</div>	<!-- optional-photo-gallery-->	
						<?php endif; ?>								


						
					</article> <!-- end article -->
					<?php
					//insurance content type
					$args = array(
						'post_type'=>'insurance',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$insurances = new WP_Query( $args );
					if($insurances->have_posts()) : 
					?>
						<div id="insurance-section" class="tb-pad-90 contact-page-ins">
							<div class="row">
								<div class="small-12 columns">
									<h2 class="white-color text-center">
										We Accept These Insurances and More
									</h2>
								</div>
							</div>
							<div class="tb-pad-20">
								<div class="row small-up-2 medium-up-2 large-up-4">
									<?php 
									while($insurances->have_posts()) : 
										$insurances->the_post();	
										$title = get_the_title();
										$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
										?>
										<div class="column" style="padding-bottom: 20px;">
											<img class="insurance-logo" alt="<?php echo $title; ?>" src="<?php echo $feat_image ?>"/>
										</div>
										<?php
										
										
									endwhile;
									?>
								</div><!-- end row -->
							</div>
							<div class="row">
								<div class="large-12 columns text-center"><a href="<?php echo site_url(); ?>/admissions/the-abcs-of-insurance-coverage/" class="button hollow">Learn More</a></div>
							</div>
						</div>

						<?php wp_reset_postdata(); ?>


					<?php endif; ?>
					<?php //comments_template('',true); ?>
					
					<?php endwhile; ?>			
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				 <!-- end #content -->

<?php get_footer(); ?>