<?php 

//service callout shortcode
add_shortcode( 'place_content_callout', 'place_content_callout_func' );
function place_content_callout_func(){
	global $post;
    //required callout fields
	$title_one = get_field('content_icon_one_title', $post->ID);
    $image_one = get_field('content_icon_one_image', $post->ID);
    
    //second service icone
	$title_two = get_field('content_icon_two_title', $post->ID);
    $image_two = get_field('content_icon_two_image', $post->ID);
    
    //second service icone
	$title_three = get_field('content_icon_three_title', $post->ID);
    $image_three = get_field('content_icon_three_image', $post->ID);
    
    //if any of the above fields are empty, then this is no good
    if( (empty($title_one) && empty($image_one)) || (empty($title_two) && empty($image_two)) || (empty($title_three) && empty($image_three)) ){
        return false;
    } else{
        $final_array = array();
        $first_block = array(
            'title'=>$title_one,
            'image'=>$image_one['url'],
            'description'=> get_field('content_icon_one_description', $post->ID),
            'button_url'=> get_field('content_icon_one_button_url', $post->ID),
            'button_text'=> get_field('content_icon_one_button_text', $post->ID),
        );
        $final_array[] = $first_block;
        
        $second_block = array(
            'title'=>$title_two,
            'image'=>$image_two['url'],
            'description'=> get_field('content_icon_two_description', $post->ID),
            'button_url'=> get_field('content_icon_two_button_url', $post->ID),
            'button_text'=> get_field('content_icon_two_button_text', $post->ID),
        );
        $final_array[] = $second_block;
        
        $third_block = array(
            'title'=>$title_three,
            'image'=>$image_three['url'],
            'description'=> get_field('content_icon_three_description', $post->ID),
            'button_url'=> get_field('content_icon_three_button_url', $post->ID),
            'button_text'=> get_field('content_icon_three_button_text', $post->ID),
        );
        $final_array[] = $third_block;
    }
	 $final_final_array = array(
											'title' => get_field('content_icons_title', $post->ID),
											'data' => $final_array,
											'post_type' => get_post_type($post)
										 );
    $html = content_callout_templating($final_final_array);
    return $html;
}

function content_callout_templating($content_callout_array){
	$html = '';
	if($content_callout_array['post_type'] == 'page') {
			$html .= '</div>';
		$html .= '</div>';
	} else {
		$html .= '<div class="tb-pad-30">';
	}
	$html .= '<div id="services-section">';
		if(!empty($content_callout_array['title'])) {
			$html .= '<div class="row">';
				$html .= '<div class="large-12 columns text-center">';
					$html .= '<h2>'.$content_callout_array['title'].'</h2>';
				$html .= '</div>';
			$html .= '</div>';
		}
		$html .= '<div class="row expanded columns medium-up-3">';
			foreach($content_callout_array['data'] AS $cc_block) {
				$html .= '<div class="column text-center">';
					if(!empty($cc_block['title'])) {
						$html .= '<h3>'.$cc_block['title'].'</h3>';
					}
					if(!empty($cc_block['image'])) {
						$html .= '<div class="content-icon-box">
										<img src="'.$cc_block['image'].'" alt="">
									</div>';
					}
					if(!empty($cc_block['description'])) {
						$html .= '<div class="content-callout-description"><div class="row"><div class="large-'.($content_callout_array['post_type'] == 'page' ? '8' : '12' ).' columns large-centered"><p class="">'.$cc_block['description'].'</p></div></div></div>';
					}
					if(!empty($cc_block['button_url'])) {
						$html .= '<a href="'.$cc_block['button_url'].'" class="button hollow secondary">';
						$html .= (!empty($cc_block['button_text'])) ? $cc_block['button_text'] : 'Learn More' ;
						$html .= '</a>';
					}
				$html .= '</div>';
			}
		$html .= '</div>';
	$html .= '</div>';
	if($content_callout_array['post_type'] == 'page') {
		$html .= '<div class="clearfix row">';
			$html .= '<div class="medium-12 columns clearfix ">';
	} else {
		$html .= '</div>';
	}
	return $html;
}




	




// Add Shortcode
function place_news_work_func() {
	global $post;
	$latest_news = get_latest_news();
	$latest_news = $latest_news[0];
	$work_snippet = get_work_snippet();
	$arr = array(
		'latest_news'=>$latest_news,
		'work_snippet'=>$work_snippet
	);
	return render_news_work_banner($arr);
}
add_shortcode( 'place_news_work', 'place_news_work_func' );

function render_news_work_banner($arr){
	//emma form
	ob_start();
	 $form = ob_get_contents();
	$form = str_replace('<label class="emma-form-label" for="emma-email"> Email</label>','', $form);
ob_end_clean();
	
	$html = '<div id="news-work-section"> <div class="row tb-pad-60">';
		$html .= '<div class="medium-6 columns">';
			$html .= '<div class="nws-news">';
				$html .= '<h3> Latest News </h3>';
				$html .= '<h5 style="margin-bottom: 5px;">'.$arr['latest_news']['title'].'</h5>';
				$html .= '<p>'.$arr['latest_news']['excerpt'].'</p>';
				$html .= '<div>';
					if( !empty($arr['latest_news']['featured_image']) ) {
						$html .= '<div class="nws-news-image" style="background-image: url(\''.$arr['latest_news']['featured_image'].'\');"></div>';
					}
				$html .= '</div>';
				$html .= '<a href="'.$arr['latest_news']['url'].'" class="button hollow large">Learn More</a>';
			$html .= '</div>';
		$html .= '</div>';
		$html .= '<div class="medium-6 columns">';
			$html .= '<div class="nws-work">';
				$html .= '<h3> Work With us</h3>';
				$html .= '<p>'.$arr['work_snippet']['excerpt'].'</p>';
					if( !empty($arr['work_snippet']['featured_image']) ) {
						$html .= '<img src="'.$arr['work_snippet']['featured_image'].'" alt="'.$arr['work_snippet']['title'].'" />';
					}
				$html .= '<a href="'.get_permalink(6576).'" class="button hollow large">Careers Page</a>';
				$html .= '<p>&nbsp;</p>';
				$html .=$form;
			$html .= '</div>';
		$html .= '</div>';
	$html .= '</div>';
	$html .= '</div>';
	return $html;
}




// Add Shortcode
function place_wide_banner_func() {
	global $post;
	$image = get_field('banner_image', $post->ID);
	if($image){
		$src = $image['url'];
	} else{
		$src = 'http://skywood.foxfuellabs.com/wp-content/uploads/2016/02/futuristic_car-wallpaper-1920x1080.jpg';
	}
	$arr = array(
		'img'=>$src,
		'title'=>get_field('banner_text', $post->ID)
	);
	return render_wide_banner($arr);
}
add_shortcode( 'place_wide_banner', 'place_wide_banner_func' );

function render_wide_banner($arr){
	$html =  '<div class="wide-banner">';
		$html .= '<div class="title">'.$arr['title'].'</div>';
		$html .='<img class="img-responsive" src="'.$arr['img'].'"/>';
	$html .= '</div>';
	return $html;
	
}



function render_image_banner_func(){
	global $post;
	$callout = get_field('image_banner_text', $post->ID);
	$callout = apply_filters('the_content', $callout);
	$callout_image_bg_prop = get_field('image_banner_bg_prop', $post->ID);
	$callout_image = get_field('image_banner_image', $post->ID);
	$callout_image = (!empty($callout_image)) ? $callout_image['url'] : '' ;
	$callout_img_tag = (!empty($callout_image)) ? '<img src="'.$callout_image.'" alt="" class="image_banner_responsive_image">' : '' ;
	if(get_post_type ($post) == 'page') {
		$html = '
					</div>
				</div>
				<div class="image_banner_callout">
					<div class="row" data-equalizer data-equalize-on="large">
						<div class="small-12 large-'.(empty($callout_image) ? '12' : '7' ).' columns" data-equalizer-watch>
							<div class="image_banner_text tb-pad-60">
								'.$callout_img_tag.$callout.'
							</div>
						</div>';
		if(!empty($callout_image)) {
			$html .=  '<div class="show-for-large large-5 columns" style="background: url('.$callout_image.') no-repeat center center / '.$callout_image_bg_prop.'; " data-equalizer-watch>
							</div>';
		}
		$html .= '</div>
				</div>
				<div class="clearfix row">
					<div class="large-12 columns clearfix " role="main">';
	} else {
		$html = '
				<div class="image_banner_callout">
					<div class="row" data-equalizer data-equalize-on="large">
						<div class="small-12 large-'.(empty($callout_image) ? '12' : '7' ).' columns" data-equalizer-watch>
							<div class="image_banner_text tb-pad-60">
								'.$callout_img_tag.$callout.'
							</div>
						</div>';
		if(!empty($callout_image)) {
			$html .=  '<div class="show-for-large large-5 columns" style="background: url('.$callout_image.') no-repeat center center / '.$callout_image_bg_prop.'; " data-equalizer-watch>
							</div>';
		}
		$html .= '</div>
				</div>';
	}
	return $html;
	
}
add_shortcode( 'render_image_banner', 'render_image_banner_func' );

function render_plain_banner_func(){
	global $post;
	$callout = get_field('plain_banner_text', $post->ID);
	if(get_post_type ($post) == 'page') {
			$html = '
					</div>
				</div>
				<div class="plane_banner_callout">
					<div class="row">
						<div class="small-12 medium-12 columns">
							<div class="plain_banner_text">
								'.$callout.'
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="medium-12 columns clearfix " role="main">';
	} else {
		$html = '
					</div>
				</div>
				<div class="plane_banner_callout">
					<div class="row">
						<div class="small-12 medium-12 columns">
							<div class="plain_banner_text">
								'.$callout.'
							</div>
						</div>
					</div>
				</div>
				<div class="row" data-equalizer="outer" data-equalize-on="large">
					<div class="large-3 columns sidebar-one show-for-large float-right" data-equalizer-watch="outer"></div>
					<div class="large-9 columns tb-pad-30 float-left" role="main" data-equalizer-watch="outer">';
	}
	return $html;
	
}
add_shortcode( 'render_plain_banner', 'render_plain_banner_func' );

//USED ON THE FRONT END
function split_content($on_wide_banner = false) {
    
    
    //$content = preg_split('/<span id="split"><\/span>/', get_the_content() );
	$content[] = get_the_content();
	
	if($on_wide_banner) {
		$content = explode('[place_wide_banner]',get_the_content() );
	} else {
		$content = explode('[add_resource_callout]',get_the_content() );
	}
	
    for($c = 0, $csize = count($content); $c < $csize; $c++) {
        $content[$c] = apply_filters('the_content', $content[$c]);
    }
    return $content;
}

function content_for_list_view(){
	//$content = get_the_excerpt();
	$content = get_the_content();
	str_replace('[add_resource_callout]', '', $content);
	echo apply_filters('the_content', $content);
}





function add_resource_quote_func(){
	global $post;
	$quote = get_field('quote', $post->ID);
	if($quote){
		
	
	$speaker = get_field('quote_speaker', $post->ID);
	$html =  '<div class="resource-quote">';
		$html .= '<div class="quote"><blockquote>'.$quote.'</blockquote></div>';
		if($speaker){
			$html .= '<div class="speaker">'.$speaker.'</div>';
		}
		
		
	$html .= '</div>';
		return $html;
	} else{
		return '';
	}
	
}
add_shortcode( 'add_resource_quote', 'add_resource_quote_func' );


// Callout Block Shortcode - used for the page templage wirth bloxks
function place_block_one_func() {
	global $post;
	$text = get_field('block_one_text', $post->ID);
	if($text){
		$placement_class = get_field('block_one_placement', $post->ID);
		$size_class = get_field('block_one_size', $post->ID);
		$html =  '<div class="callout-block block-one '.$placement_class.' '.$size_class.'">';
			$html .= '<div class="content">'.$text.'</div>';



		$html .= '</div>';		
		return $html;
	}


}
add_shortcode( 'place_block_one', 'place_block_one_func' );

// Callout Block Shortcode
function place_block_two_func() {
	global $post;
	$text = get_field('block_two_text', $post->ID);
	if($text){
		$placement_class = get_field('block_two_placement', $post->ID);
		$size_class = get_field('block_two_size', $post->ID);
		$html =  '<div class="callout-block block-two '.$placement_class.' '.$size_class.'">';
			$html .= '<div class="content">'.$text.'</div>';



		$html .= '</div>';		
		return $html;
	}


}
add_shortcode( 'place_block_two', 'place_block_two_func' );
/*
add_shortcode( 'add_resource_quote', 'add_resource_quote_func' );
function add_resource_callout_func(){
	global $post;
	$callout = get_field('callout_banner_text', $post->ID);
	if($callout){
		
	
	
	$html =  '<div class="resource-callout">';
			$html .= $callout;
		
		
	$html .= '</div>';
		return $html;
	} else{
		return '';
	}
	
}
add_shortcode( 'add_resource_callout', 'add_resource_callout_func' );
*/


function get_faq_banner(){
	$banner = get_field('faq_banner', 'option');
	if( $banner ){
			$url = $banner['url'];
			return $url;		
	} else{
			$banner = get_field('default_banner', 'option');
			$url = $banner['url'];
			return $url;			
	}

}

function get_posts_by_tag($tag_id, $exclude_id=''){
	$args = array(
		'tag_id'=>$tag_id,
		'posts_per_page' => 3
	);
	if( !empty($exclude_id) ){
		$args['exclude'] = $exclude_id;
	}
	$the_query = new WP_Query( $args );
	$resources = array();
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
			//var_dump( $feat_image );
			if($feat_image){
				$url = $feat_image;
			} else{
				$feat_image = get_field('default_resource_image', 'option');
				$url = $feat_image['url'];
			}
			$resource = array(
				'title'=>get_the_title(),
				'featured_image'=>$url,
				'id'=>get_the_id(),
				'url'=>get_the_permalink(),
				'excerpt'=>get_the_excerpt()
			);
			$resources[] = $resource;
		}//while
	}//if
	return $resources;
}

function get_resources_from_page(){
	global $post;
	//does this page have connected resources?
	$is_show = get_field('show_connected_resources', $post->ID);
	if($is_show === false){
		return false;
	}
	$post_id = $post->ID;
	//first, lets see if the user tied posts to this page
	$connected_resources = get_field('connected_resources', $post->ID);
	//var_dump($connected_resources);
	$resources = array();
	if($connected_resources){
		foreach ( $connected_resources as $post){
			setup_postdata($post);
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
			if($feat_image){
				$url = $feat_image;
			} else{
				$feat_image = get_field('default_resource_image', 'option');
				$url = $feat_image['url'];
			}
			$resource = array(
				'title'=>get_the_title(),
				'featured_image'=>$url,
				'id'=>get_the_id(),
				'url'=>get_the_permalink(),
				'excerpt'=>get_the_excerpt()
			);
			$resources[] = $resource;
			wp_reset_postdata();
		}
		return $resources;
	} 
	//we dont' have individually chosen resources, let's doa query
	$connected_resources_tag = get_field('connected_resources_tag', $post->ID);
	$tag = get_field('connected_resources_tag');
	if( $tag ){

		$resources = get_posts_by_tag($tag_id, $post_id);
		return $resources;
		//var_dump( $resources );
	}//endif
	//global tag will be set
	$global_tag = get_field('default_tag', 'option');
	$resources = get_posts_by_tag($global_tag, $post_id);
	return $resources;
	
}//end func

function get_resources_block() {
	$resources = get_resources_from_page();
	if($resources) {
		?>
			<div id="related-to-this" class="tb-pad-60">
				<div class="row">
					<div class="large-12 columns">
						<h2>Related To This</h2>
					</div>
				</div>
				<div class="row small-up-1 medium-up-3">
					<?php foreach($resources AS $ra): ?>
						<div class="column">
							<a href="<?php echo $ra['url']; ?>" class=""><h5><?php echo $ra['title']; ?></h5></a>
							<div>
								<?php echo $ra['excerpt']; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php
	}
}



/*
add_action('admin_menu','remove_default_post_type');
function remove_default_post_type() {
remove_menu_page('edit.php');
}
*/




add_shortcode( 'place_parallax_section', 'place_parallax_section_func' );
function place_parallax_section_func(){
	global $post;
	$callout = get_field('parallax_callout_text', $post->ID);
	$callout = apply_filters('the_content', $callout);
	$callout_image = get_field('parallax_callout_image', $post->ID);
	$callout_image = (!empty($callout_image)) ? $callout_image['url'] : '' ;
	if(get_post_type($post) == 'page') {
			$html = '
					</div>
				</div>
				<div id="parallax-bg-callout" class="tb-pad-40">
					<div id="parallax-callout-bg" '.(!empty($callout_image) ? 'style="background-image: url(\''.$callout_image.'\');"' : '' ).'></div>
					<div class="row">
						<div class="small-12 columns">
							'.$callout.'
						</div>
					</div>
				</div>
				<div class="row">
					<div class="medium-12 columns clearfix tb-pad-30" role="main">';
	} else {
		$html = '
				<div id="parallax-bg-callout" class="box-pad-40">
					<div id="parallax-callout-bg" '.(!empty($callout_image) ? 'style="background-image: url(\''.$callout_image.'\');"' : '' ).'></div>
					<div class="row">
						<div class="small-12 columns">
							'.$callout.'
						</div>
					</div>
				</div>';
	}
	return $html;
}

add_shortcode( 'place_two_column', 'place_two_column_func' );
function place_two_column_func(){
	global $post;
	$callout_left = get_field('two_column_left', $post->ID);
	$callout_left = apply_filters('the_content', $callout_left);
	$callout_right = get_field('two_column_right', $post->ID);
	$callout_right = apply_filters('the_content', $callout_right);
	$html = '
			<div class="tb-pad-10">
				<div class="row">
					<div class="medium-6 columns">
						'.$callout_left.'
					</div>
					<div class="medium-6 columns">
						'.$callout_right.'
					</div>
				</div>
			</div>';
	return $html;
}

add_shortcode( 'place_three_column', 'place_three_column_func' );
function place_three_column_func(){
	global $post;
	$callout_left = get_field('three_column_left', $post->ID);
	$callout_left = apply_filters('the_content', $callout_left);
	$callout_middle = get_field('three_column_middle', $post->ID);
	$callout_middle = apply_filters('the_content', $callout_middle);
	$callout_right = get_field('three_column_right', $post->ID);
	$callout_right = apply_filters('the_content', $callout_right);
	$html = '
			<div class="tb-pad-10">
				<div class="row">
					<div class="medium-4 columns">
						'.$callout_left.'
					</div>
					<div class="medium-4 columns">
						'.$callout_middle.'
					</div>
					<div class="medium-4 columns">
						'.$callout_right.'
					</div>
				</div>
			</div>';
	return $html;
}

add_shortcode( 'place_left_bg_section', 'place_bg_image_section_func' );
function place_bg_image_section_func(){
	global $post;
	$callout = get_field('left_bg_section_text', $post->ID);
	$callout = apply_filters('the_content', $callout);
	$callout_image = get_field('left_bg_section_image', $post->ID);
	$callout_image = (!empty($callout_image)) ? $callout_image['url'] : '' ;
	$callout_bg = get_field('left_bg_section_bg_color', $post->ID);
	if(get_post_type($post) == 'page') {
			$html = '
					</div>
				</div>
				<div id="left-bg-callout" class="tb-pad-60" style="'.(!empty($callout_image) ? 'background-image: url(\''.$callout_image.'\'); ' : '' ).($callout_bg != false ? 'background-color: '.$callout_bg.';' : '' ).'">
					<div class="row">
						<div class="large-4 columns">
						
						</div>
						<div class="large-8 columns">
							'.$callout.'
						</div>
					</div>
				</div>
				<div class="row">
					<div class="medium-12 columns clearfix tb-pad-30" role="main">';
	} else {
		$html = '
				<div id="left-bg-callout" class="box-pad-40" style="'.(!empty($callout_image) ? 'background-image: url(\''.$callout_image.'\'); ' : '' ).($callout_bg != false ? 'background-color: '.$callout_bg.';' : '' ).'">
					<div class="row">
						<div class="large-4 columns">
						
						</div>
						<div class="large-8 columns">
							'.$callout.'
						</div>
					</div>
				</div>';
	}
	return $html;
}


function place_second_image_banner_func(){
	global $post;
	$callout = get_field('second_image_banner_text', $post->ID);
	$callout = apply_filters('the_content', $callout);
	$callout_image_bg_prop = get_field('second_image_banner_bg_prop', $post->ID);
	$callout_image = get_field('second_image_banner_image', $post->ID);
	$callout_image = (!empty($callout_image)) ? $callout_image['url'] : '' ;
	$callout_img_tag = (!empty($callout_image)) ? '<img src="'.$callout_image.'" alt="" class="image_banner_responsive_image">' : '' ;
	if(get_post_type ($post) == 'page') {
		$html = '
					</div>
				</div>
				<div class="image_banner_callout">
					<div class="row" data-equalizer data-equalize-on="large">
						<div class="small-12 large-'.(empty($callout_image) ? '12' : '7' ).' columns" data-equalizer-watch>
							<div class="image_banner_text tb-pad-60">
								'.$callout_img_tag.$callout.'
							</div>
						</div>';
		if(!empty($callout_image)) {
			$html .=  '<div class="show-for-large large-5 columns" style="background: url('.$callout_image.') no-repeat center center / '.$callout_image_bg_prop.'; " data-equalizer-watch>
							</div>';
		}
		$html .= '</div>
				</div>
				<div class="clearfix row">
					<div class="large-12 columns clearfix " role="main">';
	} else {
		$html = '
				<div class="image_banner_callout">
					<div class="row" data-equalizer data-equalize-on="large">
						<div class="small-12 large-'.(empty($callout_image) ? '12' : '7' ).' columns" data-equalizer-watch>
							<div class="image_banner_text tb-pad-60">
								'.$callout_img_tag.$callout.'
							</div>
						</div>';
		if(!empty($callout_image)) {
			$html .=  '<div class="show-for-large large-5 columns" style="background: url('.$callout_image.') no-repeat center center / '.$callout_image_bg_prop.'; " data-equalizer-watch>
							</div>';
		}
		$html .= '</div>
				</div>';
	}
	return $html;
	
}
add_shortcode( 'place_second_image_banner', 'place_second_image_banner_func' );


//featured image shortcode
function place_featured_image_func() {
	global $post;
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	$title = get_the_title($post->ID);
	
	if($feat_image){
		if(get_post_type ($post) == 'page' || get_post_type ($post) == 'post'){
			$html = '<img class="featured-image" alt="'.$title.'" src="'.$feat_image.'"/>';
		} else{
			return false;
		}
		
	} else{
		return false;
	}

	return $html;
}
add_shortcode( 'place_featured_image', 'place_featured_image_func' );
?>