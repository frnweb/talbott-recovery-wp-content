				<div id="call-us">

					<div class="row tb-pad-60">

						<div class="large-12 columns text-center">

							<img src="<?php echo get_template_directory_uri()  ?>/images/icon.phone.png" alt="" style="margin-bottom: 30px;">

							<h2 id="footer-phone">Call us now <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Global Footer"]');?></h2>

							<div id="call-us-address"><a href="https://goo.gl/maps/TUd9wVwTGUUTd2qy6" target="_blank"><?php echo get_field('company_address', 'option'); ?></a></div>
							
							<div>

								<ul class="menu">

									<li><a href="<?php the_field('twitter_url', 'option') ?>" target="_blank" class="social twitter"></a></li>

									<li><a href="<?php the_field('facebook_url', 'option') ?>"  target="_blank" class="social facebook"></a></li>

									<li><a href="<?php the_field('youtube_url', 'option') ?>"  target="_blank" class="social youtube"></a></li>

									<li><a href="https://www.linkedin.com/company/talbott-recovery-campus/" class="social linkedin"></a></li>

								</ul>

							</div>

						</div>

					</div>

				</div>

				<!-- Mailchimp Email Form -->
		        <div class="sl_footer__email">
		            <div class="sl_inner">
		                <?php echo do_shortcode('[email]'); ?>
		            </div>
		        </div>

			</div><!-- #content -->

		</div><!-- nav sticky -->

		<footer class="tb-pad-60">

			<div class="row">

				<div class="large-12 columns">

					<div>

						<div class="row">
							<div class="column">
							<div class="medium-10 large-11 columns show-for-medium">
								<div id="footer-nav">
									<?php wp_bootstrap_footer_links(); // Adjust using Menus in Wordpress Admin ?>
								</div>
							</div>
							<div class="medium-2 large-1 columns">
								<?php
									//certification content type
									$args = array(
										'post_type'=>'certification',
										'orderby'=>'menu_order',
										'order'=>'asc'
									);
									$certifications = new WP_Query( $args );
									if($certifications->have_posts()) : 
										?>
										<div id="certification-section" class="tb-pad-10">
											<div class="tb-pad-20">
												<div class="georgia-accreds row small-up-1 medium-up-1 large-up-1">
													<?php 
													while($certifications->have_posts()) : 
														$certifications->the_post();	
														$title = get_the_title();
														$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
														$link =  get_field('certification_url');
														?>
														<div class="column">
															<a href="<?php echo $link ?>" title="<?php echo $title ?>">
																<img class="certification-logo" alt="<?php echo $title; ?>" src="<?php echo $feat_image ?>"/>
															</a>
														</div>
														<?php
														
														
													endwhile;
													?>
												</div><!-- end row -->
											</div>
										</div>
								
<?php wp_reset_postdata(); ?>


										<?php 
									endif;
								?>
							</div>
						</div>
						<div class="row footer-accreditations">
						<div class="columns medium-3 large-3">
							<a id="bbblink" class="rbhzbum" href="https://www.bbb.org/us/ga/atlanta/profile/drug-abuse-information/talbott-recovery-campus-0443-27663584" title="Foundations Recovery Network, LLC, Drug Abuse Information, Brentwood, TN" style="display: block;position: relative;overflow: hidden; width: 150px; height: 57px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="https://seal-nashville.bbb.org/logo/rbhzbum/foundations-recovery-network-37038606.png" width="300" height="57" alt="Foundations Recovery Network, LLC, Drug Abuse Information, Brentwood, TN" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); (function(){var s=document.createElement('script');s.src=bbbprotocol + 'seal-nashville.bbb.org' + unescape('%2Flogo%2Ffoundations-recovery-network-37038606.js');s.type='text/javascript';s.async=true;var st=document.getElementsByTagName('script');st=st[st.length-1];var pt=st.parentNode;pt.insertBefore(s,pt.nextSibling);})();</script>
						</div>
						<div class="columns medium-3 large-3">
							<a href="https://www.qualitycheck.org/quality-report/?bsnId=1202">
								<img class="joint-commission" alt="Joint Commission Quality Check " src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/JointCommission.gif">
							</a>
						</div>
			            <div class="columns medium-3 large-3">
					      <script src="https://static.legitscript.com/seals/3417926.js"></script>
					    </div>
					    <div class="columns medium-3 large-3">
						<a href="https://www.naatp.org/resources/addiction-industry-directory/2378/talbott-recovery-campus" target="_blank"><img src="https://www.naatp.org//civicrm/file?reset=1&id=4087&eid=201&fcs=1f26e81149c63e3ea4639b16e1ccf9e6a66df075bd944bb7ddb8b7024507c576_1585039261_87600"></a>
					    </div>
						</div><!-- /.row -->
						<p class="joint-commission-font">Talbott Recovery Campus is dedicated to providing the highest level of quality care to our patients. The Joint Commission’s gold seal of approval on our website shows that we have demonstrated compliance to the most stringent standards of performance, and we take pride in our accreditation. The Joint Commission standards deal with organization quality, safety-of-care issues and the safety of the environment in which care is provided. If you have concerns about your care, we would like to hear from you. Please contact us at <a href="tel:678.251.3100">678.251.3100</a>. If you do not feel that your concerns have been addressed adequately, you may contact The Joint Commission at: Division of Accreditation Operations, Office of Quality Monitoring, The Joint Commission, One Renaissance Boulevard, Oakbrook Terrace, IL 60181, Telephone: <a href="tel:800-994-6610">800-994-6610</a></p>
						<p>Physicians are on the medical staff of Talbott Recovery, but, with limited exceptions, are independent practitioners who are not employees or agents of Talbott Recovery. The facility shall not be liable for actions or treatments provided by physicians. Model representations of real patients are shown. Actual patients cannot be divulged due to HIPAA regulations. For language assistance, disability accommodations and the non-discrimination notice, visit our website. Affordable transportation options available upon request</p>
						<div class="text-center">
						<?php echo do_shortcode('[frn_footer]');?>						
						</div>
					</div>
					</div>

				</div>

			</div>

		</footer>

		<!--[if lt IE 7 ]>

  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>

  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>

		<![endif]-->

		

		<?php wp_footer(); // js scripts are inserted using this function ?>



		<script src="<?php echo get_template_directory_uri()  ?>/js/app.js?v=1.02"></script>

		<?php if ( is_front_page() ): ?>

			<script src="<?php echo get_template_directory_uri()  ?>/library/js/fancybox/source/jquery.fancybox.js"></script>
			
			<script type="text/javascript" src="<?php echo get_template_directory_uri()  ?>/plugins/Remodal-1.1.0/dist/remodal.min.js"></script>

			<link type="text/css" href="<?php echo get_template_directory_uri()  ?>/library/js/fancybox/source/jquery.fancybox.css" rel="stylesheet"></link>

			<script src="<?php echo get_template_directory_uri()  ?>/library/js/fancybox/source/helpers/jquery.fancybox-media.js"></script>

			<script src="<?php echo get_template_directory_uri()  ?>/library/js/talbott-homepage.js?v=1.3"></script>
			<script>
				jQuery(document).ready(function($){
									   
$('#home-banner-play-btn > a, #m-home-banner-play-btn > a').magnificPopup({
type:'iframe',
	callbacks:{
		open: function(){
			if (typeof ga === 'function') ga('send', 'event', 'Videos', 'Player Opens');
		}
	},
iframe: {
	markup: '<div class="mfp-iframe-scaler">'+
			'<div class="mfp-close"></div>'+
			'<iframe class="mfp-iframe" frameborder="0" allowfullscreen src=""></iframe>'+
		  '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button

	patterns: {
		youtube: {
			index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

			id: 'v=', // String that splits URL in a two parts, second part should be %id%
			// Or null - full URL will be returned
			// Or a function that should return %id%, for example:
			// id: function(url) { return 'parsed id'; }

			src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0' // URL that will be set as a source for iframe.
		}
	},
	srcAction: 'iframe_src', // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
}
});
					});
			</script>


		<?php endif; ?>


<?php 

/// Assessment Scripts
global $post; 
/// NOTE: JS expects the live environment URL and assessment will not work in test or DEV locations but will work once made live. 6/13/17

?>
<?php if( $post->ID == 6520): //Treatment page ID ONLY ?>

<script id="assessment-template" type="text/x-handlebars-template">

	<div id="assessment" class="">

		<div class="questions clearfix">

		{{#each questions }}

			<div class="question question_{{ @index }}" data-index="{{ @index }}">

				<div class="title">

					{{{ this.title.rendered }}}

				</div>

				<div class="buttons">

					<div class="row tb-pad-20">

						<div class="small-6 medium-5 columns">

							<button type="button" class="button large expanded  answer" data-answer="yes" data-index="{{ @index }}">

								Yes

							</button>

						</div>

						<div class="small-6 medium-5 columns">

							<button type="button" class="button large expanded  answer" data-answer="no" data-index="{{ @index }}">

								No

							</button>

						</div>

						<div class="medium-2 columns show-for-medium"></div>

					</div>

				</div>

				<div class="message">

				</div>

				<div class="nav-buttons hide">

					{{#if @first }}

						

					{{else}}

						<button type="button" class="button nav" data-action="back" data-index="{{ @index }}">

							Back

						</button>						

					{{/if}}

					{{#if @last }}

						

					{{else}}

						<button type="button" class="button nav" data-action="next" data-index="{{ @index }}">

							Next

						</button>	

					{{/if}}

				</div>

				<div class="clearfix"></div>

			</div>



		{{/each}}

		</div><!-- questions -->

		<div style="clear:both"></div>

		<div class="row" id="assess-progress-bar">

			<div class="medium-4 large-3 columns show-for-medium">

				<p style="margin-bottom: 0; line-height: .8; font-size: 1.2em;">Your Progress</p>

			</div>

			<div class="medium-8 large-9 columns">

				<div class="progress">

					<div class="progress-meter"></div>

				</div>

			</div>

			

		</div>

		<div class="clearfix"></div>

		<div id="assess-arrow-shell" class="show-for-medium">

			<div id="assess-arrow-outer"></div>

			<div id="assess-arrow-inner"></div>

		</div>

	</div>

</script>

<script id="question-template" type="text/x-handlebars-template">

	<div class="question ">

			<h3>

			

			</h3>

	</div>

</script>

<script id="result-template" type="text/x-handlebars-template">

	<div class="question" data-index="{{ total }}">

		<div class="title">Results</div>

		<div class="">

			<p>{{{message}}}</p>

			<p id="assess-phone"><?php echo do_shortcode('[frn_phone action="Phone Clicks in Assessment Results"]');?></p>

		</div>

	</div>

</script>

	    <script src="<?php echo get_template_directory_uri()  ?>/library/js/handlebars.min.js"></script>
	    <script src="<?php echo get_template_directory_uri()  ?>/library/js/jquery.cycle2.js"></script>
	    <script src="<?php echo get_template_directory_uri()  ?>/library/js/assessments.js?v=1.2"></script>

<?php 
	endif; 
	//ends script addition for the Assessment -- only placed on the Treatment page 
?>


<?php if(  get_post_type($post->ID) == 'location'):?>
	    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIMD1YT4lCGjwcXIu53LWV1QArc4xQdN0"></script>
	    <script type="text/javascript" src="<?php echo get_template_directory_uri()  ?>/library/js/location-map.js"></script>
<?php endif; ?>


<?php if(  $post->ID == 6612):?>
    	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIMD1YT4lCGjwcXIu53LWV1QArc4xQdN0"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri()  ?>/library/js/contact-map.js?v=1.1"></script>
<?php endif; ?>
        
	</body>

</html>