<?php get_header(); ?>
			
			
			
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						//page views
						wpb_set_post_views(get_the_ID());


					?>
					<header>
						<div class="row">
							<div class="medium-12 columns">
								<h1  class="tb-pad-60"><?php the_title(); ?></h1>
							</div>
						</div>
					</header> <!-- end article header -->
					
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article"  >
						
						<div class="clearfix row" >
							
							
							<div id="main" class="large-12 columns tb-pad-60 float-left" style="padding-top: 0;" role="main" >
								<section class="post_content clearfix">
									<?php 
									date_default_timezone_set ( "Etc/UTC" );
									$start_time = get_field('start_time');
									$end_time = get_field('end_time');
									
									//add five hours
									$hours = 0; //60*60*5; 

									//upgraded ACF to PRO 8/21/17, which changed how ACF saves and interprets dates/times
										// Free saved dates/times as unix timestamp
										// Pro saves dates according to the setting in their system
										// in functions.php file there is a function that filters ACF dates and converts to unix timestamp, so the detection is unnecessary but included to reduce errors in the future if it's forgotten
									//checks if saved data is unix timestamp or a more traditional date/time
									if((string)(int)$start_time === (string)$start_time) {
							        	$start_time=$start_time;
							        	$end_time=$end_time;
							        }
							        else {
							        	$start_time=strtotime($start_time);
							        	$end_time=strtotime($end_time);
							        }

									$date1 = date('l', $start_time);
									$date2 = date('M jS, Y', $start_time);
									$display_start_time = date('g:ia', $start_time);
									$display_end_time = date('g:ia', $end_time);
									
									?>
									<div class="seminar-details text-center">

											<div class="date">
												<h4><?php echo $date1.'<br>'.$date2; ?></h4>
											</div>
											<div class="time">
												<h6><?php echo $display_start_time ?> - <?php echo $display_end_time ?> <?//=date_default_timezone_get();?></h6>
											</div>	
											<?php $reg_link = get_field('registration_url') ?>
											<?php if( $reg_link ): ?>
												<a href="<?php echo $reg_link ?>" class="button hollow secondary expanded" target="_blank" style="margin-bottom: 0;" >Register</a>
											<?php endif; ?>
									</div>

									<div id="latest-news-featured-image"><?php the_post_thumbnail( 'full' ); ?></div>
									<?php the_content(); ?>
									<?php wp_link_pages(); ?>
									
									
									<?php 
									// only show edit button if user has permission to edit posts
									if( $user_level > 0 ) { 
									?>
										<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","wpbootstrap"); ?></a>
									<?php } ?>
								</section> <!-- end article section -->
							</div> <!-- end #main -->
								
						</div>
						
					</article> <!-- end article -->
					
					
					<?php //comments_template('',true); ?>
					
					<?php endwhile; ?>			
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				 <!-- end #content -->

<?php get_footer(); ?>