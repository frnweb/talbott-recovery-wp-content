<?php get_header(); ?>
			
			
			
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						//page views
						wpb_set_post_views(get_the_ID());


					?>
					<header>
						<div class="row tb-pad-40">
							<div class="medium-12 columns">
								<h1 class="single-title" style="" itemprop="headline"><?php the_title(); ?></h1>
							</div>
						</div>
						
					</header> <!-- end article header -->
					
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
						<div class="clearfix row" >
							
							
							<div id="main" class="large-12 columns tb-pad-40 float-left" style="padding-top: 0;" role="main" >
								<section class="post_content clearfix" itemprop="articleBody">
									<div id="latest-news-featured-image"><?php the_post_thumbnail( 'full' ); ?></div>
									<?php the_content(); ?>
									<?php wp_link_pages(); ?>
									
									
									<?php 
									// only show edit button if user has permission to edit posts
									if( $user_level > 0 ) { 
									?>
										<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","wpbootstrap"); ?></a>
									<?php } ?>
								</section> <!-- end article section -->
							</div> <!-- end #main -->
								
						</div>
						
					</article> <!-- end article -->
					
					
					<?php //comments_template('',true); ?>
					
					<?php endwhile; ?>			
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				 <!-- end #content -->

<?php get_footer(); ?>