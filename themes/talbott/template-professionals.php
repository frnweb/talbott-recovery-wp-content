<?php
/*
Template Name: Professionals Page Template
*/
?>
<?php get_header(); 

/*Pulling in all ACF Fields*/

//Billboard
$billboard_heading = get_field('billboard_heading');
$billboard_image = get_field('billboard_image');
$billboard_video = get_field('video_source');

//Blockquote
$quote = get_field('quote');
$cite = get_field('cite');

//Duo
$duo_text = get_field('duo_text');
$duo_image = get_field('duo_image');

//Staff
$staff = get_field('staff');
$staff_heading = get_field('staff_heading');
$staff_text = get_field('staff_text');
$staff_button_text = get_field('staff_button_text');
$staff_button_link = get_field('staff_button_link');

//Accordion
$accordion_heading = get_field('accordion_heading');

//Blockquote with Image
$quote_2 = get_field('quote_2');
$cite_2 = get_field('cite_2');
$quote_image = get_field('quote_image');

//Locations
$locations_heading = get_field('locations_heading');
$locations_text = get_field('locations_text');
$locations_callout = get_field('locations_callout');

//WYSIWYG
$wysiwyg = get_field('wysiwyg');

//Contact
$contact_text = get_field('contact_text');
$contact_link = get_field('contact_link');

//Continuing Care
$care_wysiwyg = get_field('care_wysiwyg');
$care_image = get_field('care_image');
$care_name = get_field('care_name');
$care_bio = get_field('care_bio');

//Aftercare
$aftercare_heading = get_field('aftercare_heading');
$aftercare_text = get_field('aftercare_text');
$aftercare_wysiwyg = get_field('aftercare_wysiwyg');

//CTA Callout
$callout_image = get_field('callout_image');
$callout_text = get_field('callout_text');
?>

<!-- Main Content -->
<div id="sl_professionals" class="sl_main_content">

    <!-- BILLBOARD -->
    <section class="sl_module sl_billboard" style="background-image: url(<?php echo $billboard_image ?>)">
        <div class="sl_inner">
            <div class="sl_billboard__card sl_card">
                <h1><?php echo $billboard_heading ?></h1>
                <div class="sl_button-group">
                    <?php echo do_shortcode('[frn_phone class="sl_button sl_button--primary" ga_phone_location="Phone Clicks in Professionals Page" ]'); ?>
                    <?php if ($billboard_video) echo '<a class="sl_button sl_button--border" data-open="professionals_video">Play Video</a>' ; ?>
                </div><!-- /.sl_button-group -->
            </div><!-- /.sl_billboard__card -->
        </div><!-- /.sl_inner -->
    </section>
    <!-- END BILLBOARD -->

    <!-- BLOCKQUOTE -->
    <section class="sl_module sl_quote">
        <div class="sl_inner">
            <blockquote><?php echo $quote ?>
                <cite><?php echo $cite ?></cite>
            </blockquote>     
        </div><!-- /.sl_inner -->
    </section>
    <!-- END BLOCKQUOTE -->

    <!-- DUO -->
    <section class="sl_module sl_duo">
            <div class="sl_inner">
                <div class="row">
                    <div class="columns sl_duo__media">
                            <div class="sl_duo__image" style="background-image: url(<?php echo $duo_image ?>)"></div>
                    </div><!-- /.sl_duo__media -->
                    <div class="columns sl_duo__content medium-8">
                        <div class="sl_duo__card">
                            <?php echo $duo_text ?>
                        </div><!-- /.sl_duo__card -->
                    </div><!-- /.sl_duo__content -->
                </div><!-- /.row -->
            </div><!-- /.sw_inner -->
    </section>
    <!-- END DUO -->

    <!-- STAFF MODULE -->
    <section class="sl_module sl_staff">
        <div class="sl_inner">
            <h2><?php echo $staff_heading ?></h2>
            <p><?php echo $staff_text ?></p>
            <div class="sl_staff__grid row small-up-1 medium-up-2 large-up-3">
                <?php
                /* Staff Loop */
                foreach( $staff as $staff_member) {
                setup_postdata($staff_member);

                // Staff Info
                $name = get_the_title($staff_member->ID);
                $image = get_the_post_thumbnail_url($staff_member->ID, 'full');
                $url = get_permalink($staff_member->ID);
                $job_title = get_field('job_title', $staff_member->ID);

                echo '<div class="sl_cell column">';
                    echo '<div class="sl_staff__card">';
                        echo '<div class="sl_staff__card__image" style="background-image: url(' . $image . ')"></div>';
                        echo '<div class="sl_staff__card__content">';
                            echo '<h3>' . $name . '</h3>';
                            echo '<p>' . $job_title . '</p>';
                            echo '<a class="sl_button sl_button--small" href="' . $url . '">Read Bio</a>';
                        echo '</div>';//end .sl_staff__card__content
                    echo '</div>';//end .sl_staff__card
                echo '</div>';//end .sl_cell

                }?>
            </div>  
            <div class="sl_staff__cta">
                <a class="sl_button sl_button--secondary" href="<?php echo $staff_button_link ?>"><?php echo $staff_button_text ?></a>
            </div><!-- /.sl_staff__cta -->
        </div><!-- /.sl_inner -->
    </section>
    <!-- END STAFF MODULE -->


    <!-- ACCORDION MODULE -->
    <section class="sl_module sl_dropdown">
            <div class="sl_inner">
                <h2><?php echo $accordion_heading ?></h2>
                <ul class="accordion sl_dropdown__accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">

                    <?php
                    /* Accordion Loop */
                    if( have_rows('accordion') ) : 

                        // loop accordion items
                        while ( have_rows('accordion') ) : the_row();

                            //Accordion Fields
                            $title = get_sub_field('accordion_title');
                            $content = get_sub_field('accordion_content');


                            echo '<li class="accordion-item sl_dropdown__item" data-accordion-item>';
                                echo '<a class="accordion-title sl_dropdown__title">' . $title . '</a>';
                                echo '<div class="accordion-content sl_dropdown__content" data-tab-content>';
                                    echo $content;
                                echo '</div>'; //end .accordion-content
                            echo '</li>'; //end .accordion-item

                        endwhile;

                    endif;
                    /* End Accordion Loop */
                    ?>

                </ul>
            </div><!-- sl_inner -->
    </section>
    <!-- END ACCORDION MODULE -->

    <!-- BLOCKQUOTE WITH IMAGE -->
    <section class="sl_module sl_quote sl_quote--duo">
        <div class="sl_inner">
            <div class="sl_row row">
                <div class="sl_quote__content columns medium-7">
                    <blockquote>
                        <?php echo $quote_2 ?>
                        <cite><?php echo $cite_2 ?></cite>
                    </blockquote>
                </div>
                <div class="sl_quote__media columns medium-5">
                    <img class="sl_quote__image" alt="<?php echo $quote_image['alt'] ?>" src="<?php echo $quote_image['url'] ?>" />
                </div>
            </div>
        </div><!-- /.sl_inner -->
    </section>
    <!-- END BLOCK QUOTE WITH IMAGE -->

    <!-- CTA MODULE -->
    <section class="sl_module sl_cta">
        <div class="sl_inner">
            <div class="sl_cta__card">
                <h2>Start your journey today</h2>
                <div class="sl_button-group">
                    <?php echo do_shortcode('[frn_phone class="sl_button sl_button--phone" ga_phone_location="Phone Clicks in Professionals Page Contact Module" ]'); ?>
                    <a class="sl_button sl_button--border-dark" href="mailto:Talbottintake@uhsinc.com">Email Us</a>
                </div><!-- sl_button-group -->
            </div><!-- sl_cta__card -->
        </div><!-- /.sl_inner -->
    </section>
    <!-- END CTA MODULE -->

    <!-- LOCATIONS MODULE -->
    <section class="sl_module sl_locations">
        <div class="sl_inner">
        <div class="sl_row row small-up-1 medium-up-2 large-up-3">
            <div class="sl_locations__header columns">
                <h2><?php echo $locations_heading ?></h2>
                <?php echo $locations_text ?>
            </div><!-- sl_locations__header -->


            <?php
            /* Locations Loop */
            if( have_rows('locations') ) : 

                // loop locations items
                while ( have_rows('locations') ) : the_row();

                    //Locations Fields
                    $image = get_sub_field('image');
                    $name = get_sub_field('name');
                    $address = get_sub_field('address');
                    $link = get_sub_field('link');


                    echo '<div class="sl_locations__items columns">';
                        echo '<div class="sl_card sl_locations__card">';
                            echo '<div class="sl_locations__card__image" style="background-image: url(' . $image . ')"></div>';
                            echo '<div class="sl_locations__card__content">';
                                echo '<h3>' . $name . '</h3>';
                                echo '<p>' . $address . '</p>';
                                echo '<a class="sl_button sl_button--simple" href="' . $link . '">Learn More</a>';
                            echo '</div>';//end .s_locations__content
                        echo '</div>';//end .sl_locations__card
                    echo '</div>';// end .sl_locations__items

                endwhile;

            endif;
            /* End Locations Loop */
            ?>

            </div><!-- /.sl_row -->

            <div class="sl_locations__callout"> 
                <?php echo $locations_callout ?>
            </div><!-- /.sl_locations__callout -->       
        </div><!-- /.sl_inner -->
    </section>
    <!-- END LOCATIONS MODULE -->

    <!-- WYSIWYG MODULE -->
    <section class="sl_module sl_wysiwyg">
        <div class="sl_inner">
            <?php echo $wysiwyg ?>
        </div><!-- /.sl_inner -->
    </section>
    <!-- END WYSIWYG MODULE -->

    <!-- CONTACT MODULE -->
    <section class="sl_contact">
        <div class="sl_inner">
            <div class="sl_contact__card">
                <a class="sl_contact__link" href="<?php echo $contact_link ?>"><?php echo $contact_text ?></a>
            </div><!-- /.sl_contact__card -->
        </div><!-- /.sl_inner -->
    </section>
     <!-- END CONTACT MODULE -->

     <!-- CONTINUING CARE -->
    <section class="sl_module sl_continuing-care">
        <div class="sl_inner">
            <?php echo $care_wysiwyg ?>
            <!-- <div class="sl_continuing-care__card">
                <div class="sl_row row">
                    <div class="sl_continuing-care__card__media columns medium-3 large-2" style="background-image: url(<?php echo $care_image ?>)"></div>
                    <div class="sl_continuing-care__card__content columns medium-9 large-10">
                        <h4><?php echo $care_name ?></h4>
                        <?php echo $care_bio ?>
                    </div>
                </div>
            </div> --><!-- /.sl_continuing-care__card -->
        </div><!-- /.sl_inner -->
    </section>
    <!-- END CONTINUING CARE -->

    <!-- AFTER CARE -->
    <section class="sl_module sl_aftercare">
        <div class="sl_inner">
            <h2><?php echo $aftercare_heading ?></h2>
            <?php echo $aftercare_text ?>
            <?php
            /* Services Loop */
            if( have_rows('service_items') ) : 
                echo '<div class="sl_aftercare__services">';
                echo '<h3>Alumni services for healthcare professionals include:</h3>';
                echo '<div class="sl_row row medium-up-2 small-up-1">';
                // loop accordion items
                while ( have_rows('service_items') ) : the_row();

                    //Accordion Fields
                    $image = get_sub_field('image');
                    $text = get_sub_field('text');

                    echo '<div class="sl_aftercare__items columns">';
                        echo '<div class="sl_card sl_aftercare__card">';
                            echo '<div class="sl_aftercare__card__image">';
                                echo '<img alt="' . $image['alt'] .'" src="' . $image['url'] . '"/>';
                            echo '</div>';
                            echo '<div class="sl_aftercare__card__content">';
                                echo '<p>' . $text . '</p>';
                            echo '</div>';//end .s_aftercare__content
                        echo '</div>';//end .sl_aftercare__card
                    echo '</div>';// end .sl_aftercare__items

                endwhile;
                echo '</div>';//end .row
                echo '</div>';//end .sl_aftercare__services
            endif;
            /* End Services Loop */
            ?>
            <?php echo $aftercare_wysiwyg ?>
        </div><!-- /.sl_inner -->
    </section>
    <!-- END AFTER CARE -->

    <section class="sl_module sl_callout-cta">
        <div class="sl_inner sl_inner--expanded">
            <div class="sl_row row">
                <div class="sl_callout-cta__media columns medium-4" style="background-image: url(<?php echo $callout_image ?>)"></div> 
                <div class="sl_callout-cta__content columns medium-8">
                    <h2><?php echo $callout_text ?></h2>
                    <div class="sl_button-group">
                        <?php echo do_shortcode('[frn_phone class="sl_button sl_button--phone" ga_phone_location="Phone Clicks in Professionals Page Bottom CTA" ]'); ?>
                        <a class="sl_button sl_button--border" href="mailto:Talbottintake@uhsinc.com">Email Us</a>
                    </div><!-- sl_button-group -->
                </div><!-- /.sl_callout-cta__content -->
            </div><!-- /.row -->
        </div><!-- /.sl_inner -->
    </section>
</div>

<!-- Popup Video -->
<?php if ($billboard_video) ?>
<div class="reveal sl_reveal sl_reveal--video" id="professionals_video" data-reveal data-reset-on-close="true">
    <div class="responsive-embed widescreen">
        <iframe width="560" height="315" src="" data-src="<?php echo $billboard_video ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span class="fa fa-times"></span>
    </button>
</div>
; ?>
<?php get_footer(); ?>