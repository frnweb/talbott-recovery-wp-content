<?php get_header(); ?>
<?php 
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
					'posts_per_page' => 10,
					'paged' => $paged,
					'orderby' => 'meta_value_num',
					'meta_key'=>'start_time',
					'order' => 'ASC',
					'post_type' =>'seminar',

					
					'meta_query'=>array(
						'key'=>'end_time',
						'value'=> time(),
						'compare' => '>=', 
						'type'		=> 'NUMERIC',
					)

				);
				query_posts( $args );

?>			
			<div class="clearfix row" >
				
				
				
				<div id="main" class="large-12 columns clearfix float-left" role="main" >
					<div class="tb-pad-40" style="padding-top: 0;">
					
						<div class="page-header"><h1>Seminars</h1></div>

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							
							<header>
								
								<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								

							
							</header> <!-- end article header -->
							
							<section class="post_content">
								<?php 
									$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
									
									date_default_timezone_set ( "Etc/UTC" );		
									$start_time = get_field('start_time');
									//add five hours
									$hours = 0; //60*60*5;
									$date = date('l, F jS Y', strtotime($start_time)+$hours);
									$display_start_time = date('g:ia', strtotime($start_time)+$hours);
									$end_time = get_field('end_time');
									$display_end_time = date('g:ia', strtotime($end_time)+$hours);

												
									if( !empty($feat_image)){
										?>
											<div class="row seminar">
												<div class="medium-4 columns">
													<img alt="" src="<?php echo $feat_image ?>"/>
												</div>
												<div class="medium-8 columns">
													<?php the_field('teaser_content'); ?>
													<?php $reg_link = get_field('registration_url') ?>
													<div class="date">
														<?php echo $date?>
													</div>
													<div class="time">
														<?php echo $display_start_time ?> - <?php echo $display_end_time ?> 
													</div>
													<div class="btn-shell">
														<?php if( $reg_link ): ?>
															<a href="<?php echo $reg_link ?>" target="_blank" class="button" >Register</a>
														<?php endif; ?>
														<a href="<?php the_permalink() ?>" class="button hollow" >Read More</a>
													</div>
												</div>
											</div>	<!-- row -->									
										<?php
									} else{
										?>
										<div class="row seminar">
											<div class="medium-12 columns">
												<?php the_field('teaser_content'); ?>

												<?php $reg_link = get_field('registration_url') ?>
												<div class="date">
													<?php echo $date?>
												</div>
												<div class="time">
													<?php echo $display_start_time ?> - <?php echo $display_end_time ?> 
												</div>
													<div class="btn-shell">
														<?php if( $reg_link ): ?>
															<a href="<?php echo $reg_link ?>" target="_blank" class="button" >Register</a>
														<?php endif; ?>
														<a href="<?php the_permalink() ?>" class="button hollow" >Read More</a>
													</div>
												
												
											</div>										
										</div>	<!-- row -->
										<?php 
									}
								
								?>
								

								
						
							</section> <!-- end article section -->
							
							<footer>
						
								
							</footer> <!-- end article footer -->
						
						</article> <!-- end article -->
						
						<?php endwhile; ?>	
						
						<?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>
							
							<?php wp_bootstrap_page_navi(); // use the page navi function ?>
							
						<?php } else { // if it is disabled, display regular wp prev & next links ?>
							<nav class="wp-prev-next">
								<ul class="clearfix">
									<li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
									<li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
								</ul>
							</nav>
						<?php } ?>			
						
						<?php else : ?>
						
						<!-- this area shows up if there are no results -->
						
						<article id="post-not-found">
							 <header>
								<h3><?php _e("2017 CE Seminar schedule coming soon!", "wpbootstrap"); //_e("Not Found", "wpbootstrap"); ?></h3>
							 </header>
							 <section class="post_content">
								<p><?php //_e("2017 CE Seminar schedule coming soon!", "wpbootstrap"); ?></p>

								<div class="row">
								<div class="small-12 columns" style="height:300px;">
									<?php //get_search_form(); ?>
								</div>
							</div>
							 </section>
							 <footer>
							 </footer>
						</article>
						
						<?php endif; ?>
					</div><!-- pad -->
				</div> <!-- end #main -->
    			
    			
    
			</div> <!-- end #content -->

<?php get_footer(); ?>