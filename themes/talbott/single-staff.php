<?php get_header(); ?>
			
			
			
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						//page views
						wpb_set_post_views(get_the_ID());


					?>

					<!--<header>
						<div class="clearfix row">
							<div class="medium-12 columns">
								<h1 itemprop="headline" class="tb-pad-60"><?php the_title(); ?></h1>
							</div>
						</div>
					</header>-->
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" >
						
						<div class="clearfix row" >
							
							
							<div id="main" class="large-12 columns tb-pad-60 float-left" style="" role="main" >
								<div class="row">
									<div class="medium-4 columns">
										<div id="staff-featured-image"><?php the_post_thumbnail( 'full' ); ?></div>
									</div>
									<div class="medium-8 columns">
										<section class="post_content clearfix" >
											<h1  class=""><?php the_title(); ?></h1>
											<h4><?php echo get_field('job_title'); ?></h4>
											<div id="staff-featured-image-mobile"><?php the_post_thumbnail( 'medium' ); ?></div>
											<?php the_content(); ?>
											<?php wp_link_pages(); ?>
											<p><a href="/our-team" class="button hollow">View All Team Members</a></p>
											
											<?php 
											// only show edit button if user has permission to edit posts
											if( $user_level > 0 ) { 
											?>
												<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","wpbootstrap"); ?></a>
											<?php } ?>
										</section> <!-- end article section -->
									</div>
								</div>
								
							</div> <!-- end #main -->
								
						</div>
						
					</article> <!-- end article -->
					
					
					<?php //comments_template('',true); ?>
					
					<?php endwhile; ?>			
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				 <!-- end #content -->

<?php get_footer(); ?>