<?php

// Add Translation Option
load_theme_textdomain( 'wpbootstrap', TEMPLATEPATH.'/languages' );
$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable( $locale_file ) ) require_once( $locale_file );

// Clean up the WordPress Head
if( !function_exists( "wp_bootstrap_head_cleanup" ) ) {  
  function wp_bootstrap_head_cleanup() {
    // remove header links
    remove_action( 'wp_head', 'feed_links_extra', 3 );                    // Category Feeds
    remove_action( 'wp_head', 'feed_links', 2 );                          // Post and Comment Feeds
    remove_action( 'wp_head', 'rsd_link' );                               // EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' );                       // Windows Live Writer
    remove_action( 'wp_head', 'index_rel_link' );                         // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );            // previous link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );             // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // Links for Adjacent Posts
    remove_action( 'wp_head', 'wp_generator' );                           // WP version
  }
}
// Launch operation cleanup
add_action( 'init', 'wp_bootstrap_head_cleanup' );

// remove WP version from RSS
if( !function_exists( "wp_bootstrap_rss_version" ) ) {  
  function wp_bootstrap_rss_version() { return ''; }
}
add_filter( 'the_generator', 'wp_bootstrap_rss_version' );

// Remove the […] in a Read More link
if( !function_exists( "wp_bootstrap_excerpt_more" ) ) {  
  function wp_bootstrap_excerpt_more( $more ) {
    global $post;
    return '...  <div style="margin-top: 20px;"><a href="'. get_permalink($post->ID) . '" class="more-link button secondary" title="Read '.get_the_title($post->ID).'">Read more &raquo;</a></div>';
  }
}
add_filter('excerpt_more', 'wp_bootstrap_excerpt_more');

// Add WP 3+ Functions & Theme Support
if( !function_exists( "wp_bootstrap_theme_support" ) ) {  
  function wp_bootstrap_theme_support() {
    add_theme_support( 'post-thumbnails' );      // wp thumbnails (sizes handled in functions.php)
    set_post_thumbnail_size( 125, 125, true );   // default thumb size
    add_theme_support( 'custom-background' );  // wp custom background
    add_theme_support( 'automatic-feed-links' ); // rss

    // Add post format support - if these are not needed, comment them out
    add_theme_support( 'post-formats',      // post formats
      array( 
        'aside',   // title less blurb
        'gallery', // gallery of images
        'link',    // quick link to other site
        'image',   // an image
        'quote',   // a quick quote
        'status',  // a Facebook like status update
        'video',   // video 
        'audio',   // audio
        'chat'     // chat transcript 
      )
    );  

    add_theme_support( 'menus' );            // wp menus
    
    register_nav_menus(                      // wp3+ menus
      array( 
        'main_nav' => 'The Main Menu',   // main nav in header
        'footer_links' => 'Footer Links', // secondary nav in footer
		'expanded_nav' => 'The Expanded Menu',   // expanded menu
      )
    );  
  }
}
// launching this stuff after theme setup
add_action( 'after_setup_theme','wp_bootstrap_theme_support' );

function wp_bootstrap_main_nav() {
  // Display the WordPress menu if available
  wp_nav_menu( 
    array( 
      'menu' => 'main_nav', /* menu name */
      'menu_class' => 'menu',
      'theme_location' => 'main_nav', /* where in the theme it's assigned */
      'container' => 'false', /* container class */
      'fallback_cb' => 'wp_bootstrap_main_nav_fallback', /* menu fallback */
      'walker' => new Bootstrap_walker()
    )
  );
}

function wp_bootstrap_footer_links() { 
  // Display the WordPress menu if available
  wp_nav_menu(
    array(
      'menu' => 'footer_links', /* menu name */
      'theme_location' => 'footer_links', /* where in the theme it's assigned */
      'container_class' => 'footer-links clearfix', /* container class */
      'fallback_cb' => 'wp_bootstrap_footer_links_fallback' /* menu fallback */
    )
  );
}

function wp_bootstrap_expanded_nav() {
  // Display the WordPress menu if available
  wp_nav_menu( 
    array( 
      'menu' => 'expanded_nav', /* menu name */
      'menu_class' => 'mm-nav',
      'theme_location' => 'expanded_nav', /* where in the theme it's assigned */
      'container' => 'false', /* container class */
      'fallback_cb' => 'wp_bootstrap_expanded_links_fallback', /* menu fallback */
      'walker' => new Foundation_mega_walker()
    )
  );
}

// this is the fallback for header menu
function wp_bootstrap_main_nav_fallback() { 
  /* you can put a default here if you like */ 
}

// this is the fallback for footer menu
function wp_bootstrap_footer_links_fallback() { 
  /* you can put a default here if you like */ 
}
// this is the fallback for expanded menu
function wp_bootstrap_expanded_links_fallback() { 
  /* you can put a default here if you like */ 
}
// Shortcodes
require_once('library/shortcodes.php');

// Admin Functions (commented out by default)
// require_once('library/admin.php');         // custom admin functions

// Custom Backend Footer
add_filter('admin_footer_text', 'wp_bootstrap_custom_admin_footer');
function wp_bootstrap_custom_admin_footer() {
	echo '<span id="footer-thankyou">Developed by <a href="http://www.foxfuelcreative.com/" target="_blank">FoxFuel Creative</a></span>.';
}

// adding it to the admin area
add_filter('admin_footer_text', 'wp_bootstrap_custom_admin_footer');

// Set content width
if ( ! isset( $content_width ) ) $content_width = 1400;

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'wpbs-featured', 780, 300, true );
add_image_size( 'wpbs-featured-home', 970, 311, true);
add_image_size( 'wpbs-featured-carousel', 970, 400, true);


/* 
to add more sizes, simply copy a line from above 
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image, 
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function wp_bootstrap_register_sidebars() {
  register_sidebar(array(
  	'id' => 'sidebar1',
  	'name' => 'Main Sidebar',
  	'description' => 'Used on every page BUT the homepage page template.',
  	'before_widget' => '<div id="%1$s" class="widget %2$s">',
  	'after_widget' => '</div>',
  	'before_title' => '<h4 class="widgettitle">',
  	'after_title' => '</h4>',
  ));
    
  register_sidebar(array(
  	'id' => 'sidebar2',
  	'name' => 'Homepage Sidebar',
  	'description' => 'Used only on the homepage page template.',
  	'before_widget' => '<div id="%1$s" class="widget %2$s">',
  	'after_widget' => '</div>',
  	'before_title' => '<h4 class="widgettitle">',
  	'after_title' => '</h4>',
  ));
    
  register_sidebar(array(
    'id' => 'footer1',
    'name' => 'Footer 1',
    'before_widget' => '<div id="%1$s" class="widget col-sm-4 %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'footer2',
    'name' => 'Footer 2',
    'before_widget' => '<div id="%1$s" class="widget col-sm-4 %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'footer3',
    'name' => 'Footer 3',
    'before_widget' => '<div id="%1$s" class="widget col-sm-4 %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));
    
    
  /* 
  to add more sidebars or widgetized areas, just copy
  and edit the above sidebar code. In order to call 
  your new sidebar just use the following code:
  
  Just change the name to whatever your new
  sidebar's id is, for example:
  
  To call the sidebar in your template, you can just copy
  the sidebar.php file and rename it to your sidebar's name.
  So using the above example, it would be:
  sidebar-sidebar2.php
  
  */
} // don't remove this bracket!
add_action( 'widgets_init', 'wp_bootstrap_register_sidebars' );

/************* COMMENT LAYOUT *********************/
		
// Comment Layout
function wp_bootstrap_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<div class="comment-author vcard clearfix">
				<div class="avatar col-sm-3">
					<?php echo get_avatar( $comment, $size='75' ); ?>
				</div>
				<div class="col-sm-9 comment-text">
					<?php printf('<h4>%s</h4>', get_comment_author_link()) ?>
					<?php edit_comment_link(__('Edit','wpbootstrap'),'<span class="edit-comment btn btn-sm btn-info"><i class="glyphicon-white glyphicon-pencil"></i>','</span>') ?>
                    
                    <?php if ($comment->comment_approved == '0') : ?>
       					<div class="alert-message success">
          				<p><?php _e('Your comment is awaiting moderation.','wpbootstrap') ?></p>
          				</div>
					<?php endif; ?>
                    
                    <?php comment_text() ?>
                    
                    <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time('F jS, Y'); ?> </a></time>
                    
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
			</div>
		</article>
    <!-- </li> is added by wordpress automatically -->
<?php
} // don't remove this bracket!

// Display trackbacks/pings callback function
function list_pings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment;
?>
        <li id="comment-<?php comment_ID(); ?>"><i class="icon icon-share-alt"></i>&nbsp;<?php comment_author_link(); ?>
<?php 

}

/************* SEARCH FORM LAYOUT *****************/

/****************** password protected post form *****/

add_filter( 'the_password_form', 'wp_bootstrap_custom_password_form' );

function wp_bootstrap_custom_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$o = '<div class="clearfix"><form class="protected-post-form" action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">
	' . '<p>' . __( "This post is password protected. To view it please enter your password below:" ,'wpbootstrap') . '</p>' . '
	<label for="' . $label . '">' . __( "Password:" ,'wpbootstrap') . ' </label><div class="input-append"><input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" name="Submit" class="btn btn-primary" value="' . esc_attr__( "Submit",'wpbootstrap' ) . '" /></div>
	</form></div>
	';
	return $o;
}

/*********** update standard wp tag cloud widget so it looks better ************/

add_filter( 'widget_tag_cloud_args', 'wp_bootstrap_my_widget_tag_cloud_args' );

function wp_bootstrap_my_widget_tag_cloud_args( $args ) {
	$args['number'] = 20; // show less tags
	$args['largest'] = 9.75; // make largest and smallest the same - i don't like the varying font-size look
	$args['smallest'] = 9.75;
	$args['unit'] = 'px';
	return $args;
}

// filter tag clould output so that it can be styled by CSS
function wp_bootstrap_add_tag_class( $taglinks ) {
    $tags = explode('</a>', $taglinks);
    $regex = "#(.*tag-link[-])(.*)(' title.*)#e";

    foreach( $tags as $tag ) {
    	$tagn[] = preg_replace($regex, "('$1$2 label tag-'.get_tag($2)->slug.'$3')", $tag );
    }

    $taglinks = implode('</a>', $tagn);

    return $taglinks;
}

add_action( 'wp_tag_cloud', 'wp_bootstrap_add_tag_class' );

add_filter( 'wp_tag_cloud','wp_bootstrap_wp_tag_cloud_filter', 10, 2) ;

function wp_bootstrap_wp_tag_cloud_filter( $return, $args )
{
  return '<div id="tag-cloud">' . $return . '</div>';
}

// Enable shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

// Disable jump in 'read more' link
function wp_bootstrap_remove_more_jump_link( $link ) {
	$offset = strpos($link, '#more-');
	if ( $offset ) {
		$end = strpos( $link, '"',$offset );
	}
	if ( $end ) {
		$link = substr_replace( $link, '', $offset, $end-$offset );
	}
	return $link;
}
add_filter( 'the_content_more_link', 'wp_bootstrap_remove_more_jump_link' );

// Remove height/width attributes on images so they can be responsive
add_filter( 'post_thumbnail_html', 'wp_bootstrap_remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'wp_bootstrap_remove_thumbnail_dimensions', 10 );

function wp_bootstrap_remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// Add the Meta Box to the homepage template
function wp_bootstrap_add_homepage_meta_box() {  
	global $post;

	// Only add homepage meta box if template being used is the homepage template
	// $post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : "");
	$post_id = $post->ID;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

	if ( $template_file == 'page-homepage.php' ){
	    add_meta_box(  
	        'homepage_meta_box', // $id  
	        'Optional Homepage Tagline', // $title  
	        'wp_bootstrap_show_homepage_meta_box', // $callback  
	        'page', // $page  
	        'normal', // $context  
	        'high'); // $priority  
    }
}

add_action( 'add_meta_boxes', 'wp_bootstrap_add_homepage_meta_box' );

// Field Array  
$prefix = 'custom_';  
$custom_meta_fields = array(  
    array(  
        'label'=> 'Homepage tagline area',  
        'desc'  => 'Displayed underneath page title. Only used on homepage template. HTML can be used.',  
        'id'    => $prefix.'tagline',  
        'type'  => 'textarea' 
    )  
);  

// The Homepage Meta Box Callback  
function wp_bootstrap_show_homepage_meta_box() {  
  global $custom_meta_fields, $post;

  // Use nonce for verification
  wp_nonce_field( basename( __FILE__ ), 'wpbs_nonce' );
    
  // Begin the field table and loop
  echo '<table class="form-table">';

  foreach ( $custom_meta_fields as $field ) {
      // get value of this field if it exists for this post  
      $meta = get_post_meta($post->ID, $field['id'], true);  
      // begin a table row with  
      echo '<tr> 
              <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
              <td>';  
              switch($field['type']) {  
                  // text  
                  case 'text':  
                      echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="60" /> 
                          <br /><span class="description">'.$field['desc'].'</span>';  
                  break;
                  
                  // textarea  
                  case 'textarea':  
                      echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="80" rows="4">'.$meta.'</textarea> 
                          <br /><span class="description">'.$field['desc'].'</span>';  
                  break;  
              } //end switch  
      echo '</td></tr>';  
  } // end foreach  
  echo '</table>'; // end table  
}  

// Save the Data  
function wp_bootstrap_save_homepage_meta( $post_id ) {  

    global $custom_meta_fields;  
  
    // verify nonce  
    if ( !isset( $_POST['wpbs_nonce'] ) || !wp_verify_nonce($_POST['wpbs_nonce'], basename(__FILE__)) )  
        return $post_id;

    // check autosave
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    // check permissions
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) )
            return $post_id;
        } elseif ( !current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
    }
  
    // loop through fields and save the data  
    foreach ( $custom_meta_fields as $field ) {
        $old = get_post_meta( $post_id, $field['id'], true );
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta( $post_id, $field['id'], $new );
        } elseif ( '' == $new && $old ) {
            delete_post_meta( $post_id, $field['id'], $old );
        }
    } // end foreach
}
add_action( 'save_post', 'wp_bootstrap_save_homepage_meta' );

// Add thumbnail class to thumbnail links
function wp_bootstrap_add_class_attachment_link( $html ) {
    $postid = get_the_ID();
    $html = str_replace( '<a','<a class="thumbnail"',$html );
    return $html;
}
add_filter( 'wp_get_attachment_link', 'wp_bootstrap_add_class_attachment_link', 10, 1 );

// Add lead class to first paragraph
function wp_bootstrap_first_paragraph( $content ){
    global $post;

    // if we're on the homepage, don't add the lead class to the first paragraph of text
    if( is_page_template( 'page-homepage.php' ) )
        return $content;
    else
        return preg_replace('/<p([^>]+)?>/', '<p$1 class="lead">', $content, 1);
}
add_filter( 'the_content', 'wp_bootstrap_first_paragraph' );

// Menu output mods
class Bootstrap_walker extends Walker_Nav_Menu {

  function start_el(&$output, $object, $depth = 0, $args = Array(), $current_object_id = 0){

	 global $wp_query;
	 $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
	
	 $class_names = $value = '';
	
		// If the item has children, add the dropdown class for bootstrap
		if ( $args->has_children ) {
			$class_names = "dropdown ";
		}
	
		$classes = empty( $object->classes ) ? array() : (array) $object->classes;
		
		$class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object ) );
		$class_names = ' class="'. esc_attr( $class_names ) . '"';
       
   	$output .= $indent . '<li id="menu-item-'. $object->ID . '"' . $value . $class_names .'>';

   	$attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
   	$attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
   	$attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
   	$attributes .= ! empty( $object->url )        ? ' href="'   . esc_attr( $object->url        ) .'"' : '';

   	// if the item has children add these two attributes to the anchor tag
   	if ( $args->has_children ) {
		  $attributes .= ' class="dropdown-toggle" data-toggle="dropdown"';
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before .apply_filters( 'the_title', $object->title, $object->ID );
    $item_output .= $args->link_after;

    // if the item has children add the caret just before closing the anchor tag
    if ( $args->has_children ) {
    	$item_output .= '<b class="caret"></b></a>';
    }
    else {
    	$item_output .= '</a>';
    }

    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $object, $depth, $args );
  } // end start_el function
        
  function start_lvl(&$output, $depth = 0, $args = Array()) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"dropdown-menu\">\n";
  }
      
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
    $id_field = $this->db_fields['id'];
    if ( is_object( $args[0] ) ) {
        $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
    }
    return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
  }        
}
// Menu output mods
class Foundation_mega_walker extends Walker_Nav_Menu{

  function start_el(&$output, $object, $depth = 0, $args = Array(), $current_object_id = 0){

	 global $wp_query;
	 $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
	
	 $class_names = $value = '';
	
		// If the item has children, add the dropdown class for bootstrap
		if ( $args->has_children ) {
			$class_names = "has-kids ";
		}
	
		$classes = empty( $object->classes ) ? array() : (array) $object->classes;
	  	if( in_array('start-column', $classes) ){
			$output .= '<div class="medium-3 columns">';
		}
	  	if( in_array('new-column', $classes) ){
			$output .= '</div><div class="medium-3 columns">';
		}
	  	if( in_array('end-column', $classes) ){
			$output .= '</div><div class="medium-3 columns end">';
		}
		
		$class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object ) );
		$class_names = ' class="'. esc_attr( $class_names ) . '"';
       
   	$output .= $indent . '<li id="menu-item-'. $object->ID . '"' . $value . $class_names .'>';

   	$attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
   	$attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
   	$attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
   	$attributes .= ! empty( $object->url )        ? ' href="'   . esc_attr( $object->url        ) .'"' : '';

   	// if the item has children add these two attributes to the anchor tag
   	if ( $args->has_children ) {
		  $attributes .= ' class="test" ';
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before .apply_filters( 'the_title', $object->title, $object->ID );
    $item_output .= $args->link_after;

    // if the item has children add the caret just before closing the anchor tag
    if ( $args->has_children ) {
    	$item_output .= '<b class="caret"></b></a>';
    }
    else {
    	$item_output .= '</a>';
    }

    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $object, $depth, $args );
  } // end start_el function
        
  function start_lvl(&$output, $depth = 0, $args = Array()) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"mm-subnav\">\n";
  }
      
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
    $id_field = $this->db_fields['id'];
    if ( is_object( $args[0] ) ) {
        $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
    }
    return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
  }        
}

add_editor_style('editor-style.css');

function wp_bootstrap_add_active_class($classes, $item) {
	if( $item->menu_item_parent == 0 && (in_array('current-menu-item', $classes)) ) {
    $classes[] = "active";
	}
  
  return $classes;
}

// Add Twitter Bootstrap's standard 'active' class name to the active nav link item
add_filter('nav_menu_css_class', 'wp_bootstrap_add_active_class', 10, 2 );

// enqueue styles
if( !function_exists("wp_bootstrap_theme_styles") ) {  
    function wp_bootstrap_theme_styles() { 

        wp_register_style( 'wpbs', get_template_directory_uri() . '/css/app.css', array(), '1.5', 'all' );
		 wp_enqueue_style( 'wpbs' );
		/*wp_register_style( 'additional', get_template_directory_uri() . '/css/additional.css', array(), '1.4', 'all' );
        wp_enqueue_style( 'additional' );  */

        // For child themes
        wp_register_style( 'wpbs-style', get_stylesheet_directory_uri() . '/style.css', array(), '1.1', 'all' );
        wp_enqueue_style( 'wpbs-style' );
    }
}
add_action( 'wp_enqueue_scripts', 'wp_bootstrap_theme_styles' );


// enqueue javascript
if( !function_exists( "wp_bootstrap_theme_js" ) ) {  
  function wp_bootstrap_theme_js(){

    if ( !is_admin() ){
      if ( is_singular() AND comments_open() AND ( get_option( 'thread_comments' ) == 1) ) 
        wp_enqueue_script( 'comment-reply' );
    }

    // This is the full Bootstrap js distribution file. If you only use a few components that require the js files consider loading them individually instead
    wp_register_script( 'jq2', 
      get_template_directory_uri() . '/bower_components/jquery/dist/jquery.min.js', 
      array('jquery'), 
      '1.1' );
		
	 wp_register_script( 'what-input', 
      get_template_directory_uri() . '/bower_components/what-input/what-input.js', 
      array('jquery'), 
      '1.1' );

    wp_register_script( 'wpbs-js', 
      get_template_directory_uri() . '/bower_components/foundation-sites/dist/foundation.min.js',
      array('jquery'), 
      '1.4' );
  
    
  
    wp_enqueue_script( 'jq2' );
    wp_enqueue_script( 'what-input' );
	 wp_enqueue_script( 'wpbs-js' );
    
  }
}
add_action( 'wp_enqueue_scripts', 'wp_bootstrap_theme_js' );

// Get <head> <title> to behave like other themes
function wp_bootstrap_wp_title( $title, $sep ) {
  global $paged, $page;

  if ( is_feed() ) {
    return $title;
  }

  // Add the site name.
  $title .= get_bloginfo( 'name' );

  // Add the site description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) ) {
    $title = "$title $sep $site_description";
  }

  // Add a page number if necessary.
  if ( $paged >= 2 || $page >= 2 ) {
    $title = "$title $sep " . sprintf( __( 'Page %s', 'wpbootstrap' ), max( $paged, $page ) );
  }

  return $title;
}
add_filter( 'wp_title', 'wp_bootstrap_wp_title', 10, 2 );

// Related Posts Function (call using wp_bootstrap_related_posts(); )
function wp_bootstrap_related_posts() {
  echo '<ul id="bones-related-posts">';
  global $post;
  $tags = wp_get_post_tags($post->ID);
  if($tags) {
    foreach($tags as $tag) { $tag_arr .= $tag->slug . ','; }
        $args = array(
          'tag' => $tag_arr,
          'numberposts' => 5, /* you can change this to show more */
          'post__not_in' => array($post->ID)
      );
        $related_posts = get_posts($args);
        if($related_posts) {
          foreach ($related_posts as $post) : setup_postdata($post); ?>
              <li class="related_post"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
          <?php endforeach; } 
      else { ?>
            <li class="no_related_post">No Related Posts Yet!</li>
    <?php }
  }
  wp_reset_query();
  echo '</ul>';
}

// Numeric Page Navi (built into the theme by default)
function wp_bootstrap_page_navi($before = '', $after = '') {
  global $wpdb, $wp_query;
  $request = $wp_query->request;
  $posts_per_page = intval(get_query_var('posts_per_page'));
  $paged = intval(get_query_var('paged'));
  $numposts = $wp_query->found_posts;
  $max_page = $wp_query->max_num_pages;
  if ( $numposts <= $posts_per_page ) { return; }
  if(empty($paged) || $paged == 0) {
    $paged = 1;
  }
  $pages_to_show = 7;
  $pages_to_show_minus_1 = $pages_to_show-1;
  $half_page_start = floor($pages_to_show_minus_1/2);
  $half_page_end = ceil($pages_to_show_minus_1/2);
  $start_page = $paged - $half_page_start;
  if($start_page <= 0) {
    $start_page = 1;
  }
  $end_page = $paged + $half_page_end;
  if(($end_page - $start_page) != $pages_to_show_minus_1) {
    $end_page = $start_page + $pages_to_show_minus_1;
  }
  if($end_page > $max_page) {
    $start_page = $max_page - $pages_to_show_minus_1;
    $end_page = $max_page;
  }
  if($start_page <= 0) {
    $start_page = 1;
  }
    
  echo $before.'<div class="text-center"><ul class="pagination" role="navigation" aria-label="Pagination">'."";
  if ($paged > 1) {
    $first_page_text = "&laquo";
    echo '<li class="prev"><a href="'.get_pagenum_link().'" title="' . __('First','wpbootstrap') . '">'.$first_page_text.'</a></li>';
  }
    
  $prevposts = get_previous_posts_link( __('&larr; Previous','wpbootstrap') );
  if($prevposts) { echo '<li>' . $prevposts  . '</li>'; }
  else { echo '<li class="disabled"><a href="#">' . __('&larr; Previous','wpbootstrap') . '</a></li>'; }
  
  for($i = $start_page; $i  <= $end_page; $i++) {
    if($i == $paged) {
      echo '<li class="active"><a href="#">'.$i.'</a></li>';
    } else {
      echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
    }
  }
  echo '<li class="">';
  next_posts_link( __('Next &rarr;','wpbootstrap') );
  echo '</li>';
  if ($end_page < $max_page) {
    $last_page_text = "&raquo;";
    echo '<li class="next"><a href="'.get_pagenum_link($max_page).'" title="' . __('Last','wpbootstrap') . '">'.$last_page_text.'</a></li>';
  }
  echo '</ul></div>'.$after."";
}

// Remove <p> tags from around images
function wp_bootstrap_filter_ptags_on_images( $content ){
  return preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
}
add_filter( 'the_content', 'wp_bootstrap_filter_ptags_on_images' );



/*



  ___________                                    .___
 /   _____/  | _____.__.__  _  ______   ____   __| _/
 \_____  \|  |/ <   |  |\ \/ \/ /  _ \ /  _ \ / __ | 
 /        \    < \___  | \     (  <_> |  <_> ) /_/ | 
/_______  /__|_ \/ ____|  \/\_/ \____/ \____/\____ | 
        \/     \/\/                               \/ 






*/

/*------------------------------------------------------SKYWOOD FUNCTIONS---------------------------------------------------------------------------------------*/
function get_page_banner(){
	global $post;
	//does this page have a banner?
	$banner = get_field('top_banner', $post->ID);
	if($banner){
		return $banner['url'];
	} else{
		//does the parent page have a banner?
		$parent_id = $post->post_parent;
		$banner = get_field('top_banner', $parent_id);
		if ($banner){
			return $banner['url'];
		}else{
			//get this from globals;
			$banner = get_field('default_banner', 'option');
			$url = $banner['url'];
			return $url;
		}
	}
	
}

function get_post_banner(){
	global $post;
	//does this page have a banner?
	$banner = get_field('top_banner', $post->ID);
	if($banner){
		return $banner['url'];
	} else{
			$banner = get_field('default_banner', 'option');
			$url = $banner['url'];
			return $url;
	}
	
}
function add_custom_files() {
   // wp_enqueue_style( 'style-name', get_stylesheet_uri() );
	 wp_enqueue_script( 'skywood.js', get_template_directory_uri() . '/library/js/skywood.js', array(), '1.4.0', true );
 	wp_enqueue_script( 'moment.js', get_template_directory_uri() . '/library/js/moment.js', array(), '1.3.0', true );
	wp_enqueue_script( 'chat.js', get_template_directory_uri() . '/library/js/chat.js', array(), '1.3.0', true );
}
add_action( 'wp_enqueue_scripts', 'add_custom_files' );



// Add Shortcode
function place_wide_banner_func() {
	global $post;
	$image = get_field('banner_image', $post->ID);
	if($image){
		$src = $image['url'];
	} else{
		$src = 'http://skywood.foxfuellabs.com/wp-content/uploads/2016/02/futuristic_car-wallpaper-1920x1080.jpg';
	}
	$arr = array(
		'img'=>$src,
		'title'=>get_field('banner_text', $post->ID)
	);
	return render_wide_banner($arr);
}
add_shortcode( 'place_wide_banner', 'place_wide_banner_func' );

function render_wide_banner($arr){
	$html =  '<div class="wide-banner">';
		$html .= '<div class="title">'.$arr['title'].'</div>';
		$html .='<img class="img-responsive" src="'.$arr['img'].'"/>';
	$html .= '</div>';
	return $html;
	
}



function render_image_banner_func(){
	global $post;
	$callout = get_field('image_banner_text', $post->ID);
	$callout = apply_filters('the_content', $callout);
	$callout_image = get_field('image_banner_image', $post->ID);
	$callout_image = (!empty($callout_image)) ? $callout_image['url'] : '' ;
	if(get_post_type ($post) == 'page') {
		$html = '
					</div>
				</div>
				<div class="image_banner_callout">
					<div class="row" data-equalizer data-equalize-on="medium">
						<div class="small-12 medium-8 columns" data-equalizer-watch>
							<div class="image_banner_text tb-pad-60">
								'.$callout.'
							</div>
						</div>
						<div class="show-for-medium medium-4 columns" style="background: url('.$callout_image.') no-repeat center center / cover; " data-equalizer-watch>
						</div>
					</div>
				</div>
				<div class="clearfix row">
					<div class="medium-12 columns clearfix tb-pad-30" role="main">';
	} else {
		$html = '
				<div class="image_banner_callout" style="padding-left: 60px; padding-right: 60px;">
					<div class="row" data-equalizer data-equalize-on="medium">
						<div class="small-12 medium-8 columns" data-equalizer-watch>
							<div class="image_banner_text tb-pad-60">
								'.$callout.'
							</div>
						</div>
						<div class="show-for-medium medium-4 columns" style="background: url('.$callout_image.') no-repeat center center / cover; " data-equalizer-watch>
						</div>
					</div>
				</div>';
	}
	return $html;
	
}
add_shortcode( 'render_image_banner', 'render_image_banner_func' );

function render_plain_banner_func(){
	global $post;
	$callout = get_field('plain_banner_text', $post->ID);
	if(get_post_type ($post) == 'page') {
			$html = '
					</div>
				</div>
				<div class="plane_banner_callout tb-pad-60">
					<div class="row">
						<div class="small-12 medium-12 columns">
							<div class="plain_banner_text">
								'.$callout.'
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="medium-12 columns clearfix tb-pad-30" role="main">';
	} else {
		$html = '

				<div class="plane_banner_callout box-pad-60">
					<div class="row">
						<div class="small-12 medium-12 columns">
							<div class="plain_banner_text">
								'.$callout.'
							</div>
						</div>
					</div>
				</div>';
	}
	return $html;
	
}
add_shortcode( 'render_plain_banner', 'render_plain_banner_func' );

//USED ON THE FRONT END
function split_content($on_wide_banner = false) {
    
    
    //$content = preg_split('/<span id="split"><\/span>/', get_the_content() );
	$content[] = get_the_content();
	
	if($on_wide_banner) {
		$content = explode('[place_wide_banner]',get_the_content() );
	} else {
		$content = explode('[add_resource_callout]',get_the_content() );
	}
	
    for($c = 0, $csize = count($content); $c < $csize; $c++) {
        $content[$c] = apply_filters('the_content', $content[$c]);
    }
    return $content;
}

function content_for_list_view(){
	//$content = get_the_excerpt();
	$content = get_the_content();
	str_replace('[add_resource_callout]', '', $content);
	echo apply_filters('the_content', $content);
}




//add resource -> used for the resource (blog) post 

function add_resource_quote_func(){
	global $post;
	$quote = get_field('quote', $post->ID);
	if($quote){
		
	
	$speaker = get_field('quote_speaker', $post->ID);
	$html =  '<div class="resource-quote">';
		$html .= '<div class="quote"><blockquote>'.$quote.'</blockquote></div>';
		if($speaker){
			$html .= '<div class="speaker">'.$speaker.'</div>';
		}
		
		
	$html .= '</div>';
		return $html;
	} else{
		return '';
	}
	
}
add_shortcode( 'add_resource_quote', 'add_resource_quote_func' );


// Callout Block Shortcode - used for the page templage wirth bloxks
function place_block_one_func() {
	global $post;
	$text = get_field('block_one_text', $post->ID);
	if($text){
		$placement_class = get_field('block_one_placement', $post->ID);
		$size_class = get_field('block_one_size', $post->ID);
		$html =  '<div class="callout-block block-one '.$placement_class.' '.$size_class.'">';
			$html .= '<div class="content">'.$text.'</div>';



		$html .= '</div>';		
		return $html;
	}


}
add_shortcode( 'place_block_one', 'place_block_one_func' );

// Callout Block Shortcode
function place_block_two_func() {
	global $post;
	$text = get_field('block_two_text', $post->ID);
	if($text){
		$placement_class = get_field('block_two_placement', $post->ID);
		$size_class = get_field('block_two_size', $post->ID);
		$html =  '<div class="callout-block block-two '.$placement_class.' '.$size_class.'">';
			$html .= '<div class="content">'.$text.'</div>';



		$html .= '</div>';		
		return $html;
	}


}
add_shortcode( 'place_block_two', 'place_block_two_func' );
/*
add_shortcode( 'add_resource_quote', 'add_resource_quote_func' );
function add_resource_callout_func(){
	global $post;
	$callout = get_field('callout_banner_text', $post->ID);
	if($callout){
		
	
	
	$html =  '<div class="resource-callout">';
			$html .= $callout;
		
		
	$html .= '</div>';
		return $html;
	} else{
		return '';
	}
	
}
add_shortcode( 'add_resource_callout', 'add_resource_callout_func' );
*/
function get_page_id_outside_loop(){
	global $post;
	return $post->ID;
}






wpcf7_add_shortcode('assessment_data', 'wpcf7_sourceurl_shortcode_handler', true);

function wpcf7_sourceurl_shortcode_handler($tag) {
	if (!is_array($tag)) return '';

	$name = $tag['name'];
	if (empty($name)) return '';

	$html = '<input id="assessment_data" type="hidden" name="' . $name . '" value="" />';
	return $html;
}

function get_faq_banner(){
	$banner = get_field('faq_banner', 'option');
	if( $banner ){
			$url = $banner['url'];
			return $url;		
	} else{
			$banner = get_field('default_banner', 'option');
			$url = $banner['url'];
			return $url;			
	}

}

function get_posts_by_tag($tag_id, $exclude_id=''){
	$args = array(
		'tag_id'=>$tag_id,
		'posts_per_page' => 3
	);
	if( !empty($exclude_id) ){
		$args['exclude'] = $exclude_id;
	}
	$the_query = new WP_Query( $args );
	$resources = array();
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
			//var_dump( $feat_image );
			if($feat_image){
				$url = $feat_image;
			} else{
				$feat_image = get_field('default_resource_image', 'option');
				$url = $feat_image['url'];
			}
			$resource = array(
				'title'=>get_the_title(),
				'featured_image'=>$url,
				'id'=>get_the_id(),
				'url'=>get_the_permalink(),
				'excerpt'=>get_the_excerpt()
			);
			$resources[] = $resource;
		}//while
	}//if
	return $resources;
}

function get_resources_from_page(){
	global $post;
	//does this page have connected resources?
	$is_show = get_field('show_connected_resources', $post->ID);
	if($is_show === false){
		return false;
	}
	$post_id = $post->ID;
	//first, lets see if the user tied posts to this page
	$connected_resources = get_field('connected_resources', $post->ID);
	//var_dump($connected_resources);
	$resources = array();
	if($connected_resources){
		foreach ( $connected_resources as $post){
			setup_postdata($post);
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
			if($feat_image){
				$url = $feat_image;
			} else{
				$feat_image = get_field('default_resource_image', 'option');
				$url = $feat_image['url'];
			}
			$resource = array(
				'title'=>get_the_title(),
				'featured_image'=>$url,
				'id'=>get_the_id(),
				'url'=>get_the_permalink(),
				'excerpt'=>get_the_excerpt()
			);
			
			$resources[] = $resource;
			wp_reset_postdata();
		}
		
		//die();
		return $resources;
	} 
	//we dont' have individually chosen resources, let's doa query
	$connected_resources_tag = get_field('connected_resources_tag', $post->ID);
	$tag = get_field('connected_resources_tag');
	if( $tag ){
		$resources = get_posts_by_tag($tag, $post_id);
		return $resources;
		//var_dump( $resources );
	}//endif
	//global tag will be set
	$global_tag = get_field('default_tag', 'option');
	$resources = get_posts_by_tag($global_tag, $post_id);
	return $resources;
	
}//end func

function get_resources_block() {
	$resources = get_resources_from_page();
	if($resources) {
		?>
			<div id="related-to-this" class="tb-pad-60">
				<div class="row">
					<div class="large-12 columns">
						<h2>Related To This</h2>
					</div>
				</div>
				<div class="row small-up-1 medium-up-3">
					<?php foreach($resources as $ra): ?>
						<div class="column">
							<a href="<?php echo $ra['url']; ?>" class=""><h5><?php echo $ra['title']; ?></h5></a>
							<div>
								<?php echo $ra['excerpt']; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php
	}
}
function is_chat_live() {
	date_default_timezone_set('America/Chicago');
	$currentDate = new DateTime();
	if($currentDate->format('N') >= 6) {
		$start_str = get_field('weekend_chat_start_time', 'option');
		$end_str = get_field('weekend_end_time', 'option');
	} else {
		$start_str = get_field('chat_start_time', 'option');
		$end_str = get_field('chat_end_time', 'option');
	}
	
	$chat_start = DateTime::createFromFormat('H:i', '00:00');
	$chat_end = DateTime::createFromFormat('H:i', '00:00');
	
	$start_parts = explode(':', $start_str);
	$end_parts = explode(':', $end_str);
	
	$start_mod = ($start_parts[0] > 0) ? '+'.$start_parts[0].' hours ' : '' ;
	$start_mod .= ($start_parts[1] > 0) ? '+'.$start_parts[1].' minutes ' : '' ;
	
	$end_mod = ($end_parts[0] > 0) ? '+'.$end_parts[0].' hours ' : '' ;
	$end_mod .= ($end_parts[1] > 0) ? '+'.$end_parts[1].' minutes ' : '' ;
	
	$chat_start->modify($start_mod);
	$chat_end->modify($end_mod);
	
	if ($currentDate > $chat_start && $currentDate < $chat_end) {
		return true;
	} else {
		return false;
	}
}
function display_phone_number($number) {
	$stripped_number = preg_replace("/[^0-9]/","",$number);
	if(strlen($stripped_number) >= 10 && strlen($stripped_number) <= 11) {
		if(preg_match("/^1/", $stripped_number)) {
			$stripped_number = substr($stripped_number, 1);
		}
		$formatted_number = '('.substr($stripped_number, -10, 3).') '.substr($stripped_number, -7, 3).'-'.substr($stripped_number, -4, 4);
	} else {
		$formatted_number = $number;
	}
	return $formatted_number;
}
function prepare_sidebar_posts(){
	$categories = get_categories( array(
		'orderby' => 'name',
		'order'   => 'ASC'
	) );
	$category_arr = array();
	foreach ( $categories as $category ) {
			$cat = array(
				'category'=>$category,
				'posts'=>get_posts( array('category'=>$category->term_id, 'posts_per_page'=>5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC') )
			);

		$category_arr[] = $cat;
	}
	return $category_arr;
}

/*
add_action('admin_menu','remove_default_post_type');
function remove_default_post_type() {
remove_menu_page('edit.php');
}
*/



add_action( 'init', 'my_new_default_post_type', 1 );
function my_new_default_post_type() {
 
    register_post_type( 'post', array(
        'labels' => array(
            'name_admin_bar' => _x( 'Post', 'add new on admin bar' ),
        ),
        'public'  => true,
        '_builtin' => true, 
        '_edit_link' => 'post.php?post=%d', 
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array( 'slug' => '' ),
        'query_var' => false,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats' ),
    ) );
}

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function get_youtube_code($url){
	preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
	return $matches[1];

}

//ADDITIONAL IMAGE SIZES
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'extra-large', 800, 400, true ); //(cropped)
	add_image_size( 'xxl', 1200, 800, true ); //(cropped)
}

add_filter('image_size_names_choose', 'my_image_sizes');

function my_image_sizes($sizes) {
	$addsizes = array(
	"extra-large" => __( "Extra Large"),
	"xxl" => __( "XXL")
	);
	$newsizes = array_merge($sizes, $addsizes);
	return $newsizes;
}
add_shortcode( 'place_parallax_section', 'place_parallax_section_func' );
function place_parallax_section_func(){
	global $post;
	$callout = get_field('parallax_callout_text', $post->ID);
	$callout = apply_filters('the_content', $callout);
	$callout_image = get_field('parallax_callout_image', $post->ID);
	$callout_image = (!empty($callout_image)) ? $callout_image['url'] : '' ;
	if(get_post_type($post) == 'page') {
			$html = '
					</div>
				</div>
				<div id="parallax-bg-callout" class="tb-pad-40">
					<div id="parallax-callout-bg" '.(!empty($callout_image) ? 'style="background-image: url(\''.$callout_image.'\');"' : '' ).'></div>
					<div class="row">
						<div class="small-12 columns">
							'.$callout.'
						</div>
					</div>
				</div>
				<div class="row">
					<div class="medium-12 columns clearfix tb-pad-30" role="main">';
	} else {
		$html = '
				<div id="parallax-bg-callout" class="box-pad-40">
					<div id="parallax-callout-bg" '.(!empty($callout_image) ? 'style="background-image: url(\''.$callout_image.'\');"' : '' ).'></div>
					<div class="row">
						<div class="small-12 columns">
							'.$callout.'
						</div>
					</div>
				</div>';
	}
	return $html;
}

add_shortcode( 'place_two_column', 'place_two_column_func' );
function place_two_column_func(){
	global $post;
	$callout_left = get_field('two_column_left', $post->ID);
	$callout_left = apply_filters('the_content', $callout_left);
	$callout_right = get_field('two_column_right', $post->ID);
	$callout_right = apply_filters('the_content', $callout_right);
	$html = '
			<div class="tb-pad-10">
				<div class="row">
					<div class="medium-6 columns">
						'.$callout_left.'
					</div>
					<div class="medium-6 columns">
						'.$callout_right.'
					</div>
				</div>
			</div>';
	return $html;
}

add_shortcode( 'place_left_bg_section', 'place_bg_image_section_func' );
function place_bg_image_section_func(){
	global $post;
	$callout = get_field('left_bg_section_text', $post->ID);
	$callout = apply_filters('the_content', $callout);
	$callout_image = get_field('left_bg_section_image', $post->ID);
	$callout_image = (!empty($callout_image)) ? $callout_image['url'] : '' ;
	$callout_bg = get_field('left_bg_section_bg_color', $post->ID);
	if(get_post_type($post) == 'page') {
			$html = '
					</div>
				</div>
				<div id="left-bg-callout" class="tb-pad-60" style="'.(!empty($callout_image) ? 'background-image: url(\''.$callout_image.'\'); ' : '' ).($callout_bg != false ? 'background-color: '.$callout_bg.';' : '' ).'">
					<div class="row">
						<div class="large-4 columns">
						
						</div>
						<div class="large-8 columns">
							'.$callout.'
						</div>
					</div>
				</div>
				<div class="row">
					<div class="medium-12 columns clearfix tb-pad-30" role="main">';
	} else {
		$html = '
				<div id="left-bg-callout" class="box-pad-40" style="'.(!empty($callout_image) ? 'background-image: url(\''.$callout_image.'\'); ' : '' ).($callout_bg != false ? 'background-color: '.$callout_bg.';' : '' ).'">
					<div class="row">
						<div class="large-4 columns">
						
						</div>
						<div class="large-8 columns">
							'.$callout.'
						</div>
					</div>
				</div>';
	}
	return $html;
}

?>