"use strict"
import gulp from "gulp"
import cssnano from "cssnano"
import postcss from "gulp-postcss"
import uglify from "gulp-uglify"


import cleanCss from "gulp-clean-css"
import plugins from "gulp-load-plugins"
import yargs from "yargs"
import rimraf from "rimraf"
import browser from "browser-sync"
import yaml from "js-yaml"
import fs from "fs"
import named from "vinyl-named"
import webpack2 from "webpack"
import webpackStream from "webpack-stream"
import csso from "gulp-csso"
import UglifyjsWebpackPlugin from "uglifyjs-webpack-plugin"
import uglifyJsOptions from "uglify-js"
import sourcemaps from 'gulp-sourcemaps'
import imagemin from 'gulp-imagemin'

// Load all Gulp plugins into one variable
const $ = plugins()

// Load settings from configs.yml
const { PORT, PROXY, COMPATIBILITY, PATHS } = loadConfig();

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

// Build Task
gulp.task(
  "build",
  gulp.series(clean, gulp.parallel(sass, javascript, images))
)

// Default Task
gulp.task("default", gulp.series('build', server, watch))

// Delete the 'dist' folder. This happens everytime a build starts
function clean(done) {
  rimraf(PATHS.dist, done)
}

function images() {
  return gulp
  .src('src/img/**/*.{jpg,png,svg}')
  .pipe(imagemin())
  .pipe(gulp.dest(PATHS.dist + "/img"))
}


// SCSS Task
function sass() {
  return gulp
    .src("src/scss/main.scss")
    .pipe($.sourcemaps.init())
    .pipe($.sass({ includePaths: PATHS.sass }).on("error", $.sass.logError))
    .pipe($.autoprefixer({ overrideBrowserslist: COMPATIBILITY }))
    .pipe(postcss([cssnano()]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(PATHS.dist + "/css"))
    .pipe(browser.reload({ stream: true }))
}

let webpackConfig = {
  mode: "production",
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [ "@babel/preset-env" ],
            compact: false
          }
        }
      },
    ]
  },
  devtool: 'source-map',
  output: {
    filename: 'app.min.js',
  },
  performance: { hints: false },
  optimization: {
    minimizer: [
      new UglifyjsWebpackPlugin({
        sourceMap: true,
        minify(file, sourceMap) {
        const uglifyJsOptions = { 	
          mangle: true,
          compress: {
            sequences: true,
            dead_code: true,
            conditionals: true,
            booleans: true,
            unused: true,
            if_return: true,
            join_vars: true,
            drop_console: false
            }
          };

        if (sourceMap) {
            uglifyJsOptions.sourceMap = {
            content: sourceMap,
          };
        }

        return require('uglify-js').minify(file, uglifyJsOptions);
       }
      }),
    ],
  },
};

// JS Task
// Combine JavaScript into one file
// In production, the file is minified
function javascript() {
  return gulp
    .src(PATHS.entries)
    .pipe(webpackStream(webpackConfig, webpack2))
    .pipe(gulp.dest(PATHS.dist + "/js"))
}

// Browsersync
function server(done) {
  browser.init({ proxy: PROXY, port: PORT })
  done()
}

function reload(done) {
  browser.reload()
  done()
}

// Watch for changes to static assets, php files, sass, and javascript
function watch() {
  gulp.watch("**/*.php").on("all", gulp.series(reload))
  gulp.watch("src/js/**/*").on("all", gulp.series(javascript, reload))
  gulp.watch("src/scss/**/*.scss").on("all", sass)
}