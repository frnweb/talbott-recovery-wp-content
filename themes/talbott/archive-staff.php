<?php get_header(); ?>
<?php 
global $query_string;
query_posts( $query_string . '&orderby=menu_order&order=ASC&posts_per_page=100');


?>				
			<div id="staff-page" class="clearfix ">
			
				<div id="main" class="clearfix" role="main">
					<div class="row clearfix">
						<div class="medium-12 columns">
							<div class="page-header"><h1>Our Team</h1></div>
						</div>
					</div>
					
					
						
					<?php $post_count = 0; ?>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php 
							$post_count++;
							$featured_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
						?>
							
							<div class="staff-row <?php echo ($post_count %2 == 0) ? 'alt-staff' : '' ; ?>">
								<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
									<?php if ( !empty($featured_src) ): ?>
								
									<section class="">
									
										<div class="row tb-pad-40" style="<?php echo ($post_count == 1) ? 'padding-top: 0;' : '' ; ?>">
											<div class="medium-4 columns <?php echo ($post_count %2 == 0) ? 'float-right' : '' ; ?>">
												<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"	><?php the_post_thumbnail( 'full' ); ?></a>
												<?php $linkedIn = get_field('linkedin_url', $post->ID); ?>
												<?php if(!empty($linkedIn)): ?>
													<div class="tb-pad-10 text-center">
														<a href="<?php echo $linkedIn; ?>" target="_blank" class="linkedInIco"></a>
													</div>
												<?php endif; ?>
											</div>
											<div class="medium-8 columns <?php echo ($post_count %2 == 0) ? 'float-left text-right' : '' ; ?>">
												<h1 class="staff-title"><?php the_title(); ?></h1>
												<h4><?php echo get_field('job_title'); ?></h4>
												<div class="staff-excerpt-content">
													<?php the_excerpt(); ?>
													<div style="margin-top: 20px;">
														<?php 
															echo '<a href="'. get_permalink($post->ID) . '" class="more-link button hollow" title="Read '.get_the_title($post->ID).'">Read more</a>';
														?>
														
													</div>
												</div>
												<div class="staff-full-content"><?php /*the_content();*/ ?></div>
											</div>
										</div>
										<div class="clearfix"></div>
									
										
								
									</section> <!-- end article section -->
									
									<?php else:
									//the else here is now the same. it used to be different
									?>
									
									<section class="">
									
										<div class="row tb-pad-40">
											<div class="medium-4 columns <?php echo ($post_count %2 == 0) ? 'float-right' : '' ; ?>">
												<?php the_post_thumbnail( 'full' ); ?>
												<?php $linkedIn = get_field('linkedin_url', $post->ID); ?>
												<?php if(!empty($linkedIn)): ?>
													<div class="tb-pad-10 text-center">
														<a href="<?php echo $linkedIn; ?>" target="_blank" class="linkedInIco"></a>
													</div>
												<?php endif; ?>
											</div>
											<div class="medium-8 columns <?php echo ($post_count %2 == 0) ? 'float-left text-right' : '' ; ?>">
												<h1 class="staff-title"><?php the_title(); ?></h1>
												<h4><?php echo get_field('job_title'); ?></h4>
												<div class="staff-excerpt-content">
													<?php the_excerpt(); ?>
													<div style="margin-top: 20px;">
														<?php 
															echo '<a href="'. get_permalink($post->ID) . '" class="more-link button hollow" title="Read '.get_the_title($post->ID).'">Read more</a>';
														?>
														
													</div>
												</div>
												<div class="staff-full-content"><?php /*the_content();*/ ?></div>
											</div>
										</div>
										<div class="clearfix"></div>
									
										
								
									</section> <!-- end article section -->
									<?php endif; ?>
								
								</article> <!-- end article -->
							</div>
							<div class="clearfix"></div>
					<?php endwhile; ?>	
					
					<?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>
						
						<?php wp_bootstrap_page_navi(); // use the page navi function ?>

					<?php } else { // if it is disabled, display regular wp prev & next links ?>
						<nav class="wp-prev-next">
							<ul class="pager">
								<li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
								<li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
							</ul>
						</nav>
					<?php } ?>
								
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("No Posts Yet", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, What you were looking for is not here.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
					<?php  wp_reset_query(); ?>
					</div>
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->
			<div class="clearfix"></div>

<?php get_footer(); ?>