var effectsCutoff = 1024

function assessArrow() {
	if($(window).width() > 640) {
		if($('#assessment-shell').offset().top <= ($(window).scrollTop() + ($(window).outerHeight()*0.75))) {
			var aA_Offset = ($('#home-contact-form').height()-$('#home-contact-form input[type="submit"]').outerHeight())-20;
			var aA_max_offset = $('#assessment-shell').outerHeight() - (72);
			if(aA_Offset < aA_max_offset && aA_Offset > 20) {
				$('#assess-arrow-shell').animate({top:aA_Offset},{duration: 3000});
			} else {
				$('#assess-arrow-shell').css('top', 20);
			}
		}
	}
}
var degrees = 0;
var degreesAmplified = 0;
var swirlySpinning = false;
var swirlySpinningInterval = '';

function rotateSwirly() {
	if($(window).width() > effectsCutoff) {
		if(swirlySpinning == false) {
			swirlySpinning = true;
			swirlySpinningInterval = setInterval(function() {
					degrees += (0.1+degreesAmplified);
					if(degreesAmplified > -0.05) {
						degreesAmplified -= 0.01;
					}
					$('#swirly').css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
								  '-moz-transform' : 'rotate('+ degrees +'deg)',
								  '-ms-transform' : 'rotate('+ degrees +'deg)',
								  'transform' : 'rotate('+ degrees +'deg)'});
			}, 50);
		} else {
			if(degreesAmplified < 0) {
				degreesAmplified = 0.01;
			} else if(degreesAmplified < 0.1) {
				degreesAmplified += 0.01;
			}
		}
	}
}
function stopSwirly() {
	clearTimeout($.data(this, 'scrollTimer'));
	$.data(this, 'scrollTimer', setTimeout(function() {
		clearInterval(swirlySpinningInterval);
		swirlySpinning = false;
	}, 1000));
}
var scale = 1;
var oldScale = scale;
var scaleArr = [];
// function zoomTreatments() {
// 	if(!jQuery('html').hasClass('ie9')) {
// 		$('.treatment-feature-image-box').each(function(key, value){
// 			scaleArr[key]['oldScale'] = scaleArr[key]['scale'];
// 			if(($(window).scrollTop()+($(window).height()*0.50)) >= $(value).offset().top && ($(window).scrollTop()+($(window).height()*0.60)) < ($(value).offset().top + $(value).outerHeight())) {
// 				if(scaleArr[key]['scale'] == 1) {
// 					scaleArr[key]['scale'] = 1.05;
// 				}
				
// 			} else if(($(window).scrollTop()+($(window).height()*0.60)) >= ($(value).offset().top + $(value).outerHeight()) || ($(window).scrollTop()+($(window).height()*0.50)) < $(value).offset().top) {
// 				if(scaleArr[key]['scale'] > 1) {
// 					scaleArr[key]['scale'] = 1;
// 				}
// 			}
// 			if(scaleArr[key]['oldScale'] != scaleArr[key]['scale']) {
// 				$(value).find('.treatment-feature-image').css({
// 					'transform': 'scale('+scaleArr[key]['scale']+')',
// 					'-moz-transform': 'scale('+scaleArr[key]['scale']+')',
// 					'-webkit-transform': 'scale('+scaleArr[key]['scale']+')',
// 					'-o-transform': 'scale('+scaleArr[key]['scale']+')',
// 					'-ms-transform': 'scale('+scaleArr[key]['scale']+')'
// 				});
// 			}
// 		});
// 	}
	
// }
var insureanceVisible = false;
function fadeInInsuranceLogos() {
	if($(window).width() > effectsCutoff) {
		if(($(window).scrollTop()+($(window).height()*0.75)) >= ($('#insurance-section').offset().top + ($('#insurance-section').outerHeight()/2))) {
			if(insureanceVisible == false) {
				var time = 0;
				$('.insurance-logo').each(function(){
					var curIns = $(this);
					setTimeout(function(){
						curIns.animate({opacity:1.0},{duration: 500});
					}, time);
					time += 200;
				});
				insureanceVisible = true;
			}
		}
	}
}
jQuery(document).ready(function( $ ) {
	$('.fancybox-media').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			media : {},
			overlay: {
			  locked: false
			}
		}
	});
//    $('.treatment-feature-image-box').each(function(key, value){
// 		scaleArr[key] = {'scale':scale, 'oldScale':oldScale};
// 	});
	fadeInInsuranceLogos();
	// zoomTreatments();
	$(window).resize(function(){
		/*assessArrow();
		slider_fix($('#assess-progress-bar').outerHeight());*/
	});
	$(window).scroll(function(){
		/*if(($(window).scrollTop()+($(window).height()*0.75)) >= $('#assessment-shell').offset().top) {
			assessArrow();
		}*/
		if(/*$('#navStopper .sticky').hasClass('is-stuck')*/ 1==1) {
			if($(window).width() > effectsCutoff) {
				var scrolled = $(window).scrollTop()/*-$('#home-banner').offset().top)*/;
				if(scrolled < 0) {
					scrolled = 0;
				}
				//console.log(scrolled);
			}
		}
		fadeInInsuranceLogos();
		rotateSwirly();
		stopSwirly();
		// zoomTreatments();
		//parallaxBG($('#video-testimonials'), 0.25, -1);
	});
	$('#header-treatment').click(function(){ $('html,body').animate({scrollTop:$('.footer-links').offset().top}, 30000); });
});
$(window).load(function(){
	/*assessArrow();*/
});