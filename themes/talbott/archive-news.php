<?php get_header(); ?>
			
			<div class="clearfix row" >
				
				
				
				<div id="main" class="large-12 columns clearfix float-left" role="main" >
					<div class="tb-pad-40" style="padding-top: 0;">
					
						<div class="page-header"><h1>Latest News</h1></div>

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							
							<header>
								
								<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								

							
							</header> <!-- end article header -->
							
							<section class="post_content">
								<?php 
									$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
									if( !empty($feat_image)){
										?>
											<div class="row">
												<div class="medium-4 columns">
													<a href="<?php the_permalink() ?>" title=""><img alt="<?php the_title_attribute(); ?>" src="<?php echo $feat_image ?>"/></a>
												</div>
												<div class="medium-8 columns">
													<?php the_excerpt(); ?>
													<a href="<?php the_permalink() ?>" class="button hollow" >Read More</a>
												</div>
											</div>										
										<?php
									} else{
										?>
										
											<div class="medium-12 columns">
												<?php the_excerpt() ?>
												<a href="<?php the_permalink() ?>" class="button hollow" >Read More</a>
											</div>										
										
										<?php 
									}
								
								?>
								

								
						
							</section> <!-- end article section -->
							
							<footer>
						
								
							</footer> <!-- end article footer -->
						
						</article> <!-- end article -->
						
						<?php endwhile; ?>	
						
						<?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>
							
							<?php wp_bootstrap_page_navi(); // use the page navi function ?>
							
						<?php } else { // if it is disabled, display regular wp prev & next links ?>
							<nav class="wp-prev-next">
								<ul class="clearfix">
									<li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
									<li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
								</ul>
							</nav>
						<?php } ?>			
						
						<?php else : ?>
						
						<!-- this area shows up if there are no results -->
						
						<article id="post-not-found">
							 <header>
								<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
							 </header>
							 <section class="post_content">
								<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
								<div class="row">
								<div class="small-12 columns">
									<?php get_search_form(); ?>
								</div>
							</div>
							 </section>
							 <footer>
							 </footer>
						</article>
						
						<?php endif; ?>
					</div>
				</div> <!-- end #main -->
    			
    			
    
			</div> <!-- end #content -->

<?php get_footer(); ?>