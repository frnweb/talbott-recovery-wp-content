<?php get_header(); ?>
<?php 
global $query_string;
query_posts( $query_string . '&orderby=menu_order&order=ASC&posts_per_page=100');


?>	
			
			<div id="content" class="clearfix row" data-equalizer="outer" data-equalize-on="large">
			
				
				
				<div id="main" class="large-12 columns clearfix" role="main" data-equalizer-watch="outer">
				
					<div class="page-header">
					
						<h1 class="archive_title h2 tb-pad-60">
							Locations
						</h1>

					</div>
						<?php $treatment_count = 0; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php $treatment_count++; ?>
					<?php 
					
										$title = get_the_title();
										
										$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
					
					?>
					
									<div style="<?php echo ($treatment_count %2 != 0) ? '' : '' ; ?>">
										<div class="row expanded collapse" data-equalizer data-equalize-on="medium">
											<div class="medium-6 columns <?php echo ($treatment_count %2 == 0) ? 'float-right' : '' ; ?>" >
												<div class="treatment-feature-image-box" data-equalizer-watch>
													<div class="treatment-feature-image" style="background: url('<?php echo $feat_image ?>') no-repeat center center / cover; transition: all 2.5s ease; -moz-transition: all 2.5s ease; -ms-transition: all 2.5s ease; -webkit-transition: all 2.5s ease; -o-transition: all 2.5s ease;" data-equalizer-watch>
												</div>
												</div>
											</div>
											<div class="medium-6 columns <?php echo ($treatment_count %2 == 0) ? 'float-left text-right' : '' ; ?>" data-equalizer-watch>
												<div class="treatment-pad">
													<div class="<?php echo ($treatment_count %2 == 0) ? 'float-right' : '' ; ?>">
														<div class="title">
															<h3><?php echo $title; ?></h3>
														</div>
														<div class="content">
															<?php if(get_field('intro_content')): ?>
																<p><?php echo get_field('intro_content'); ?><p>
															<?php else: ?>
																<?php the_excerpt() ?>
															<?php endif; ?>
														</div>
														<div class="button-shell">
															<?php 
																
																?>
							
															<a  class="button hollow" href="<?php the_permalink() ?>">
																Learn More
															</a>
																<?php 
																
															?>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										<?php
										?>
										</div><!-- end row -->
									</div>
					
					<?php endwhile; ?>	
					
					<?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>
						
						<?php wp_bootstrap_page_navi(); // use the page navi function ?>

					<?php } else { // if it is disabled, display regular wp prev & next links ?>
						<nav class="wp-prev-next">
							<ul class="pager">
								<li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
								<li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
							</ul>
						</nav>
					<?php } ?>
								
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("No Posts Yet", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, What you were looking for is not here.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				
    
			</div> <!-- end #content -->

<?php get_footer(); ?>